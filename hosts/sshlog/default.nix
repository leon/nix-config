{ config, lib, pkgs, ... }:
{
  deployment.mem = 512;
  c3d2.hq.statistics.enable = true;

  networking = {
    hostName = "sshlog";
    firewall.allowedTCPPorts = [
      22 # not using openssh module
    ];
  };
  
  services.sshlogd.enable = true;
  services.openssh.enable = lib.mkForce false;

  services.nginx = {
    enable = true;
    additionalModules = [ pkgs.nginxModules.fancyindex ];
    virtualHosts."${config.networking.hostName}.flpk.zentralwerk.org" = {
      default = true;
      forceSSL = true;
      enableACME = true;

      locations."/.theme/" = {
        alias = pkgs.fetchFromGitHub {
          owner = "barrowclift";
          repo = "directory-theme";
          rev = "fca275a3ab1d64e8cdbff7e4d2e1d44eec924e2e";
          sha256 = "sha256-UCQbQ+tyzR/Dpa0t2cogjzBJE+IJ9KAD2dtmq3gbq/U=";
        };
      };

      locations."/" = {
        root = config.services.sshlogd.outputDir;
        extraConfig = ''
          fancyindex on;
          fancyindex_exact_size off;
          fancyindex_css_href /.theme/style.css;
        '';
      };
    };
  };
}
