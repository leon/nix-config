{ tftproots, pkgs, ... }:

{
  networking.firewall.enable = false;

  # raspberrypi boot
  services.atftpd = {
    enable = true;
    root =
      let
        netbootxyzVersion = "2.0.68";
        netbootxyz_efi = pkgs.fetchurl {
          url = "https://github.com/netbootxyz/netboot.xyz/releases/download/${netbootxyzVersion}/netboot.xyz.efi";
          sha256 = "1nr43a69wyyncyn2myxi0qxanbkizbk085vzp961g38hnllgbbig";
        };
        netbootxyz_kpxe = pkgs.fetchurl {
          url = "https://github.com/netbootxyz/netboot.xyz/releases/download/${netbootxyzVersion}/netboot.xyz.kpxe";
          sha256 = "06g0pfind9jix3y1a7g8c5q8ad95kz98vlliqxr901j9qr764kg2";
        };
      in
      pkgs.runCommand "tftproot" { } ''
        mkdir $out

        # PXE for PC
        ln -s ${netbootxyz_efi} $out/netboot.xyz.efi
        ln -s ${netbootxyz_kpxe} $out/netboot.xyz.kpxe

        # generic boot files for pis
        cp -sr ${tftproots.rpi-netboot-tftproot}/* $out/

        # dacbert
        ln -s /var/lib/nfsroot/dacbert/boot $out/3c271952
      '';
  };
}
