{ config, pkgs, ... }:

{
  networking.hostName = "oxigraph";
  system.stateVersion = "22.11";
  c3d2.hq.statistics.enable = true;
  deployment = {
    vcpu = 16;
    mem = 8192;
    needForSpeed = true;
  };

  users = {
    groups.oxigraph = {};
    users.oxigraph = {
      isSystemUser = true;
      group = "oxigraph";
      home = "/var/lib/oxigraph";
      createHome = true;
    };
  };

  systemd.services.oxigraph = {
    wantedBy = [ "multi-user.target" ];
    after = [ "network.target" ];
    serviceConfig = {
      User = "oxigraph";
      Group = "oxigraph";
      ExecStart = "${pkgs.oxigraph}/bin/oxigraph_server serve -l ${config.users.users.oxigraph.home}/data";
    };
  };

  # curl https://dumps.wikimedia.org/wikidatawiki/entities/latest-all.nt.bz2 |bzip2 -cd - | parallel -j`nproc` --pipe -L 100000 --joblog /tmp/split_log.txt --resume-failed 'F=$(mktemp /tmp/wikidata-XXXXXX); cat > $F && time curl -X POST -H 'Content-Type:application/n-triples' -T $F "http://localhost:7878/store?graph=https://wikidata.org/"; rm $F'
}
