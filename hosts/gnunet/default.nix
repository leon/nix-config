{
  system.stateVersion = "22.05";

  c3d2.hq.statistics.enable = true;
  deployment = {
    # needs to keep just its ssh key for sops-nix
    persistedShares = [ "/etc" "/var" ];
    mem = 1024;
  };

  networking = {
    hostName = "gnunet";
    firewall.enable = false;
  };

  services.gnunet = {
    enable = true;
    load = {
      # bits/s
      maxNetDownBandwidth = 1000 * 1000 * 1000;
      maxNetUpBandwidth = 1 * 1000 * 1000;
    };
  };
}
