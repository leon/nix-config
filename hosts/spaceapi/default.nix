_:

{
  c3d2.deployment.server = "server10";

  networking = {
    firewall.enable = false;
    hostName = "spaceapi";
  };

  services.spaceapi.enable = true;

  # HACK for ‘ekg-json-0.1.0.6’ nixos-22.05
  # nixpkgs.config.allowBroken = true;

  system.stateVersion = "19.03";
}
