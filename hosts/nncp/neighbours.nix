{ config, pkgs, ... }:

{
  programs.nncp.settings.neigh.emery.exec = {
    # A command to asynchronously export store paths to a remote caller.
    nix-store-export = with pkgs;
      [
        (writeScript "nncp-nix-store-export.sh" ''
          #!${bash}/bin/bash
          set -euo pipefail
          ${config.nix.package}/bin/nix-store --export $@ | \
            ${config.programs.nncp.package}/bin/nncp-exec -nice $NNCP_NICE $NNCP_SENDER nix-store-import
        '')
      ];

    # A command to import store paths from a remote caller.
    nix-store-import = [ "${config.nix.package}/bin/nix-store" "--import" ];
  };
}
