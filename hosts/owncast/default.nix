{ config, pkgs, ... }:

{
  imports = [
    ./owncast-archiver.nix
  ];

  deployment = {
    vcpu = 8;
    mem = 2048;
    persistedShares = [ "/etc" "/home" "/var" ];
    extraShares = [ {
      source = "/storage/cephfs/microvms/c3d2/config/owncast/archive";
      mountPoint = config.services.owncast-archiver.targetDir;
    } ];
  };
  c3d2.hq.statistics.enable = true;

  networking.hostName = "owncast";

  services.owncast = {
    enable = true;
    openFirewall = true;
  };

  services.nginx = {
    enable = true;
    additionalModules = [ pkgs.nginxModules.fancyindex ];
    virtualHosts."owncast.c3d2.de" = {
      default = true;
      forceSSL = true;
      enableACME = true;

      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString config.services.owncast.port}";
        proxyWebsockets = true;
      };
      locations."/archive".return = "307 /archive/";
      locations."/archive/" = {
        alias = "${config.services.owncast-archiver.targetDir}/";
        extraConfig = ''
          fancyindex on;
          fancyindex_exact_size off;
        '';
      };
    };
  };
  systemd.services.nginx.serviceConfig.ReadWritePaths = [
    config.services.owncast-archiver.targetDir
  ];

  services.owncast-archiver.enable = true;
}
