{ pkgs, ... }:

{
  imports = [ ./hardware-configuration.nix ];

  c3d2 = {
    hq.interface = "enp0s25";
    k-ot.enable = true;
    autoUpdate = true;
  };

  disko.disks = [ {
    device = "/dev/disk/by-id/ata-SSD0240S00_20201124BC41037";
    name = ""; # empty because disk was formatted before the naming convention
    withCeph = false;
    withLuks = false;
  } ];

  nix.settings = {
    cores = 4;
    max-jobs = 4;
  };

  sops = {
    age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    defaultSopsFile = ./secrets.yaml;
  };

  boot.loader = {
    efi.canTouchEfiVariables = true;
    systemd-boot.enable = true;
  };

  networking = {
    domain = "hq.c3d2.de";
    firewall = {
      allowedTCPPorts = [
        5900 # vnc
      ];
    };
    hostId = "42424242";
    hostName = "glotzbert";
    interfaces.enp0s25.useDHCP = true;
  };

  console.keyMap = "de";

  environment.systemPackages = with pkgs; [
    chromium
    firefox
    mpv
    kodi
    # tracer-game
    bevy_julia
    bevy_mandelbrot
    allcolors
  ];

  systemd.user.services.x11vnc = {
    description = "X11 VNC server";
    wantedBy = [ "graphical-session.target" ];
    partOf = [ "graphical-session.target" ];
    serviceConfig = {
      ExecStart = ''
        ${pkgs.x11vnc}/bin/x11vnc -shared -forever -passwd k-ot
      '';
      RestartSec = 3;
      Restart = "always";
    };
  };

  sound.enable = true;

  hardware = {
    opengl = {
      extraPackages = with pkgs; [
        vaapiIntel
        libvdpau-va-gl
        intel-media-driver
      ];
      extraPackages32 = with pkgs.pkgsi686Linux; [
        vaapiIntel
        libvdpau-va-gl
        intel-media-driver
      ];
    };
    pulseaudio = {
      enable = true;
      systemWide = true;
      zeroconf = {
        discovery.enable = true;
        publish.enable = true;
      };
      tcp = {
        enable = true;
        anonymousClients.allowAll = true;
      };
      extraConfig = ''
        load-module module-tunnel-sink server=pipebert.hq.c3d2.de
      '';
      extraClientConf = ''
        default-server = pipebert.hq.c3d2.de
      '';
    };
  };

  services = {
    xserver = {
      enable = true;
      layout = "de";
      xkbOptions = "eurosign:e";

      displayManager = {
        lightdm = { enable = true; };
        autoLogin = {
          enable = true;
          user = "k-ot";
        };
        defaultSession = "gnome-xorg";
      };
      desktopManager = {
        gnome.enable = true;
        kodi.enable = true;
      };
    };
  };

  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };

  users.groups."k-ot".gid = 1000;
  users.users."k-ot" = {
    group = "k-ot";
    extraGroups = [ "networkmanager" ];
  };

  system.stateVersion = "22.11"; # Did you read the comment?
}
