{  bevy-julia
, bevy-mandelbrot
, tracer
}:

final: prev:

with final; {
  allcolors = callPackage ./allcolors.nix { };

  inherit (bevy-julia.packages.${system}) bevy_julia;
  inherit (bevy-mandelbrot.packages.${system}) bevy_mandelbrot;

  bmxd = callPackage ./bmxd.nix { };

  dex-oidc = prev.dex-oidc.override {
    buildGoModule = args: buildGoModule (args // {
      patches = args.patches or [ ] ++ [
        # remember session
        (fetchpatch {
          url = "https://github.com/dexidp/dex/commit/dd0fb05386ce89c74381ce49e903cc10b987459e.patch";
          sha256 = "sha256-71py0pysgS3jDkKeqD/K4KJ821bolz/4PTjt2rDdUy8=";
        })
      ];

      vendorSha256 = "sha256-BxFiRHOGIJf3jTVtrw/QbnvG5gyfwAKQGd3IiWw5iVc=";
    });
  };

  dump1090-influxdb = callPackage ./dump1090-influxdb { };

  dump1090_rs = callPackage ./dump1090_rs.nix { };

  chromium = prev.chromium.override {
    # enable hardware accerlation with vaapi, force dark mode, detect kwallet
    commandLineArgs = "--enable-features=VaapiVideoEncoder,VaapiVideoDecoder,CanvasOopRasterization --force-dark-mode --password-store=detect";
  };

  firefox = prev.firefox.override {
    extraPolicies = {
      DisablePocket = true;
      FirefoxHome.Pocket = false;
    };
  };

  grafana = prev.grafana.overrideAttrs (_: {
    # broken with backports and take a good amount of time
    doCheck = false;
  });

  # hydra flake
  hydra = prev.hydra.overrideAttrs (oldAttrs: {
    patches = oldAttrs.patches or [ ] ++ [
      # gitea webhook support
      (fetchpatch {
        url = "https://github.com/NixOS/hydra/pull/1227/commits/750978a19232583e17620a1bd80435e957e7213a.patch";
        sha256 = "sha256-86Li0YUSVUdnw6lt6kZ56ohDRKPD13SZzukqPU1np8U=";
      })
      # fix github webhook from orgs
      (fetchpatch {
        url = "https://github.com/NixOS/hydra/commit/4d664ecb0faaf51b21673f979b543ea4694c3f1b.patch";
        sha256 = "sha256-lF5Rnz8r9ptyMLhcg/XnjiNhOK1KcLA7hi01ye4KgmI=";
      })
    ];
  });

  # hydra in nixpkgs
  hydra_unstable = prev.hydra_unstable.overrideAttrs (oldAttrs: {
    patches = oldAttrs.patches or [ ] ++ [
      # gitea webhook support
      (fetchpatch {
        url = "https://github.com/NixOS/hydra/pull/1227/commits/750978a19232583e17620a1bd80435e957e7213a.patch";
        sha256 = "sha256-86Li0YUSVUdnw6lt6kZ56ohDRKPD13SZzukqPU1np8U=";
      })
      # fix github webhook from orgs
      (fetchpatch {
        url = "https://github.com/NixOS/hydra/commit/4d664ecb0faaf51b21673f979b543ea4694c3f1b.patch";
        sha256 = "sha256-lF5Rnz8r9ptyMLhcg/XnjiNhOK1KcLA7hi01ye4KgmI=";
      })
    ];
  });

  mlat-client = python3Packages.callPackage ./mlat-client.nix { };

  nixVersions = prev.nixVersions // {
    stable = prev.nixVersions.stable.overrideAttrs (oldAttrs: {
      patches = oldAttrs.patches or [ ] ++ [
        ./nix-increase-substituter-threadpool.diff
        # request compression
        (fetchpatch {
          url = "https://github.com/NixOS/nix/pull/7712.patch";
          sha256 = "sha256-mAx2h0/r7HayvTjMMxmewaD+L4OOB2gRJaQb3JEb0rk=";
        })
      ];
    });
  };

  openssh = prev.openssh.overrideAttrs (_: {
    # takes 30 minutes
    doCheck = false;
  });

  oxigraph = callPackage ./oxigraph.nix { };

  pi-sensors = callPackage ./pi-sensors { };

  plume = callPackage ./plume { };

  readsb = callPackage ./readsb.nix { };

  telme10 = callPackage ./telme10.nix { };

  tracer-game =
    if true
    then throw "tracer-game: haddock runs on affection for 10 hours and more"
    else tracer.packages.${system}.tracer-game;

  trainbot = callPackage ./trainbot.nix { };
}
