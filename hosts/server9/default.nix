{ config, ... }:

{
  imports = [
    ./hardware-configuration.nix
  ];

  c3d2 = {
    baremetal = true;
    deployment.microvmBaseZfsDataset = "tank/storage";
    hq.statistics.enable = true;
  };

  boot = {
    loader.grub = {
      enable = true;
      version = 2;
      device = "/dev/sdc";
    };
    kernelParams = [
      "preempt=none"
      # No server/router runs any untrusted user code
      "mitigations=off"
    ];
    tmpOnTmpfs = true;
    tmpOnTmpfsSize = "80%";
  };

  networking = {
    hostName = "server9";
    hostId = "09090909";
  };

  # required by libvirtd
  security.polkit.enable = true;

  # reserve resources for libvirt VMs
  services.nomad.settings.client.reserved = {
    cpu = 2300;
    memory = 16 * 1024;
  };

  simd.arch = "westmere";

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets."machine-id" = {
      mode = "444";
      path = "/etc/machine-id";
    };
    secrets."ceph/osd.3/keyfile" = {};
  };

  skyflake.nomad.client.meta."c3d2.cpuSpeed" = "3";
  skyflake.storage.ceph.osds = [ {
    id = 3;
    fsid = "54d56ab8-fc43-4e16-886d-3c82dcc1f8fe";
    path = "/dev/zvol/tank/ceph-osd.3";
    keyfile = config.sops.secrets."ceph/osd.3/keyfile".path;
    deviceClass = "hdd";
  } ];

  system.stateVersion = "21.11";

  # TODO: enable for zw-ev and poelzi-ha until we find a better solution
  virtualisation.libvirtd = {
    enable = true;
    onShutdown = "shutdown";
  };

}
