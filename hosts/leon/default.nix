#                                               .--.
#                                               `.  \
#                                                 \  \
#                                                  .  \
#                                                  :   .
#                                                  |    .
#                                                  |    :
#                                                  |    |
#  ..._  ___                                       |    |
# `."".`''''""--..___                              |    |
# ,-\  \             ""-...__         _____________/    |
# / ` " '                    `""""""""                  .
# \                                                      L
# (>                                                      \
#/                                                         \
#\_    ___..---.                 MEOW OS                    \
#  `--'         '.                                           \
#                 .                                           \_
#                _/`.                                           `.._
#             .'     -.                                             `.
#            /     __.-Y     /''''''-...___,...--------.._            |
#           /   _."    |    /                ' .      \   '---..._    |
#          /   /      /    /                _,. '    ,/           |   |
#          \_,'     _.'   /              /''     _,-'            _|   |
#                  '     /               `-----''               /     |
#                  `...-'                                       `...-'
#--------------------------------------------------------------------------------
{ lib, pkgs, ssh-public-keys, ... }:

{
  deployment = {
    mem = 2048;
    vcpu = 4;
  };
  nix.settings.auto-optimise-store = lib.mkForce false;

  networking = {
    hostName = "leon";
    firewall.enable = true;
  };
  # enable IP routing
  boot.kernel.sysctl."net.ipv4.conf.all.forwarding" = 1;
  boot.kernel.sysctl."net.ipv4.conf.default.forwarding" = 1;

  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };
  c3d2.hq.statistics.enable = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
     wget vim python3Full python310Packages.pip python310Packages.flask nmap htop wireguard-tools docker-compose
   ];

  virtualisation.docker.enable = true;

  users.users.leon = {
    isNormalUser = true;
    extraGroups = [ "wheel" "docker" ];
    createHome = true;
    openssh.authorizedKeys.keys = ssh-public-keys.leon;
  };
  networking.nameservers = ["172.20.73.8" "9.9.9.9"];
	networking.firewall = {
    allowedTCPPorts = [ 5000 22 53 80 443 8080 12000 ];
    allowedUDPPorts = [ 53 80 8080 18900 19900 ];  
  };
#_______________________________Begin-VPN1-Server____________________________________
 
 networking.wireguard.interfaces = {
    #Interface. Trusted VPN
	vpn1 = {
      #IP address && Subnet.
      ips = [ "10.10.11.1/24" ];
      #VPN Port.
      listenPort = 18900;
      privateKeyFile = "/etc/wireguard/privatekey";
      #----------------------Start-Routing----------------------------
	  postSetup = ''
        ${pkgs.iptables}/bin/iptables -t nat -A POSTROUTING -s 10.10.11.1/24 -o flpk -j MASQUERADE
      '';

      # This undoes the above command
      postShutdown = ''
        ${pkgs.iptables}/bin/iptables -t nat -D POSTROUTING -s 10.10.11.1/24 -o flpk -j MASQUERADE
      '';
      #----------------------End-Routing----------------------------
     
	 peers = [
           # ------------------leon-------------------------.
        { 
          #leon-Mac
		  publicKey = "okZuIQ90J49IJ3s+/dZyLthvo4rR2kclmTL54Ykglio=";
          allowedIPs = [ "10.10.11.2" ];
		}
		{
		  #leon-Phone
		  publicKey= "OKv8k1ctlcYN9du/lWA65E0+nMpYOhUR8jf+rfQS+BY=";
          allowedIPs = [ "10.10.11.3" ];
        }
		{
		  #leoncloud-vm
		  publicKey = "YhcC/xMYxh7GIyndbgBZ05oE3aTJXK4T7JgZwUDyd08=";
          allowedIPs = [ "10.10.11.4" ];
		}
		{
		  #leon-Pi
		  publicKey = "TzAImxyt2by5BfCR79U5giD4rzPrUrtWWy2IvXVF+ik=";
          allowedIPs = [ "10.10.11.6" ];
		}
		{
		  #leon-windows
		  publicKey = "djNLXtdgDAD2P/14EHXG8bjcHkKvPflNZhEHq5AEcgU=";
          allowedIPs = [ "10.10.11.12" ];
		}
		  #-------------------Killow---------------------------.
		{ 
		  #Killow-Server1
	 	  publicKey= "sg2U9isTU7fFj9dv8lFepbg1EE4s7RissF6ZCZf2cGs=";
          allowedIPs = [ "10.10.11.60" ];
		}
	    {
		  #Killow-Server2
		  publicKey= "tNWJ8FFydI08Ls/faHU1mp0Uf1ZLzZke66DpL6ffoHY=";
          allowedIPs = [ "10.10.11.61" ];
	    }
	    {
		  #Gecol-docker
		  publicKey= "DiA9FKxMDziCt4sEsINIdc8RlM1hq9fA4LVwf062mAE=";
          allowedIPs = [ "10.10.11.62" ];
	    }
		{
		  #Acer-laptop
		  publicKey= "+/neUKXEHkLv+zYGw37+ckwoT3kHQIHyHRTbCtjUUA4=";
          allowedIPs = [ "10.10.11.63" ];
	    }
		{
		  #zeo-vm
		  publicKey= "aUoH+Ukmsrk4zsQj5LX/VeuXMjgTTHZ98voy6zvNf14=";
          allowedIPs = [ "10.10.11.66" ];
	    }

	   #---------------------M-dev---------------------------.
	   {
		  #M-dev-pc
		  publicKey= "l63JSZVnqWgFvKol+MlXWPnPjwq3b0L8gVkOzQz/F3k=";
          allowedIPs = [ "10.10.11.21" ];
	    }
	   #---------------------Malak-pc---------------------------.
	   {
		  #malak-pc
		  publicKey= "H5mMm7EblaSjEKLkKEn8575XeKWFgs/7LjEIpBwJ0HU=";
          allowedIPs = [ "10.10.11.33" ];
	    }
		#---------------------Ameria-pc---------------------------.
	   {
		  #amira-pc
		  publicKey= "uJYIR+BFdOauVY/wrohpufOl9ZxmQ1CbDNUVHnz0+Uc=";
          allowedIPs = [ "10.10.11.31" ];
	    }


	  ];
  };
 };
 #-----------------------------END-VPN---------------------------------

#__________________________Begin-VPN2-Server_____________________
 
networking.wireguard.interfaces = {
    #Interface. Untrusted VPN
	vpn2 = {
      #IP address && Subnet.
      ips = [ "10.10.100.1/24" ];

      #VPN Port.
      listenPort = 19900;

	  # Path to the private key file.
      #
      privateKeyFile = "/etc/wireguard/privatekey";

      peers = [
        # -----------------leon-Mac-------------------------.
        { 
          publicKey = "6GRIp7SjHyu5sgqudtgZdN9CKbV3GYtMnwgo06F4ylo=";
          allowedIPs = [ "10.10.100.0/24" ];
        }
	  ];
    };
  };
  
#-----------------------------END-VPN---------------------------------

#-----------------------------ngin-X--------------------------------

  services.nginx = {
    enable = true;
	virtualHosts."bicospacetech.c3d2.de" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "http://45.158.40.162:12000";
        proxyWebsockets = true;
      };
    };
	virtualHosts."bicospacetech.cloud.c3d2.de" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
	    proxyPass = "http://45.158.40.165:80";   
	    proxyWebsockets = true;
      };
	};
	virtualHosts."bicospacetech.stream.c3d2.de" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "http://10.10.11.4:14500";
        proxyWebsockets = true;
      };
	};
	virtualHosts."portainer.netlab.c3d2.de" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "http://10.10.11.60:9000";
        proxyWebsockets = true;
      };
	};
	virtualHosts."hospital-gly.c3d2.de" = {
      forceSSL = true;
      enableACME = true;
	locations."/" = {
        proxyPass = "http://10.10.11.21";
        proxyWebsockets = true;
      };
	};


};
#-----------------------------ngin-X--------------------------------

system.stateVersion = "22.05";

}
