{ config, ... }:

{
  imports = [
    ./hardware-configuration.nix
  ];

  c3d2 = {
    baremetal = true;
    # deployment.microvmBaseZfsDataset = "tank/storage";
    hq.statistics.enable = true;
  };

  boot = {
    loader.systemd-boot.enable = true;
    kernelParams = [
      "preempt=none"
      # No server/router runs any untrusted user code
      "mitigations=off"
    ];
    tmpOnTmpfs = true;
    tmpOnTmpfsSize = "80%";
  };

  networking = {
    hostName = "server7";
    hostId = "07070707";
  };

  simd.arch = "ivybridge"; # E5-2690 v2

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets."machine-id" = {
      mode = "444";
      path = "/etc/machine-id";
    };
    secrets."ceph/osd.5/keyfile" = {};
    secrets."ceph/osd.6/keyfile" = {};
  };

  skyflake.nomad.client.meta."c3d2.cpuSpeed" = "5";
  skyflake.storage.ceph.osds = [ {
    id = 5;
    fsid = "036260b7-6bff-4e90-a635-a18640223fe0";
    path = "/dev/server7_nvme0/ceph";
    keyfile = config.sops.secrets."ceph/osd.5/keyfile".path;
    deviceClass = "nvme";
  } {
    id = 6;
    fsid = "e4dbb8be-da42-4a85-85c9-da207b17386c";
    path = "/dev/server7_ssd0/ceph";
    keyfile = config.sops.secrets."ceph/osd.6/keyfile".path;
    deviceClass = "ssd";
  } ];

  system.stateVersion = "22.11";
}
