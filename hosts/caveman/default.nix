{ config, ... }:

{
  system.stateVersion = "22.05";

  c3d2 = {
    deployment.server = "server10";
    hq.statistics.enable = true;
  };
  microvm = {
    vcpu = 8;
    mem = 16 * 1024;
  };

  networking = {
    hostName = "caveman";
    firewall.allowedTCPPorts = [ 23 ];
  };

  services.journald.extraConfig = ''
    Storage=volatile
  '';

  services = {
    # Override default backup schedule to reduce I/O
    redis.servers.caveman.save = [
      # Every 2h if at least 1 entry changed
      [ 7200 1 ]
      # Every 30min if at least 10000 entries changed
      [ 1800 10000 ]
    ];

    caveman = {
      # leave 4 GB for caveman services
      redis.maxmemory = (config.microvm.mem - 4) * 1024 * 1024;

      hunter = {
        enable = true;
        settings = {
          prometheus_port = 9103;
          max_workers = 384;
          hosts = with builtins;
            filter isString (
              split "\n" (
                readFile ./mastodon-instances.txt
              )
            );
        };
      };
      butcher.enable = true;
      gatherer.enable = true;
      smokestack.enable = true;
    };

    nginx = {
      enable = true;
      virtualHosts."fedi.buzz" = {
        default = true;
        forceSSL = true;
        enableACME = true;
        serverAliases = [
          "www.fedi.buzz"
          "caveman.flpk.zentralwerk.org"
        ];
        locations."/".proxyPass = "http://127.0.0.1:${toString config.services.caveman.gatherer.settings.listen_port}/";
      };
    };
  };
}
