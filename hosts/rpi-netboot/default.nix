{ hostRegistry, lib, pkgs, ... }:

{
  c3d2 = {
    hq.interface = "eth0";
    hq.statistics.enable = false;
    audioServer.enable = true;
    k-ot.enable = true;
    autoUpdate = false;
    hq.journalToMqtt = false;
  };

  boot = {
    # repeat https://github.com/NixOS/nixos-hardware/blob/master/raspberry-pi/4/default.nix#L20
    # to overwrite audio module
    kernelPackages = pkgs.linuxKernel.packages.linux_rpi4;
    kernelParams = [ "verbose" "console=tty0" ];
  };

  swapDevices = [ ];

  hardware.bluetooth.enable = true;

  powerManagement.cpuFreqGovernor = lib.mkDefault "performance";

  networking = {
    hostName = "rpi-netboot";
    useDHCP = false;
    interfaces.eth0.useDHCP = true;
    firewall.enable = false;
  };

  fileSystems = {
    "/" = {
      fsType = "tmpfs";
      options = [ "mode=0755" ];
    };
    "/etc" = {
      fsType = "tmpfs";
      options = [ "mode=0755" ];
    };
     # mount the server's /nix/store
    "/nix/store" = {
      device = "${hostRegistry.nfsroot.ip4}:/nix/store";
      fsType = "nfs";
      options = [ "nfsvers=3" "proto=tcp" "nolock" "hard" "async" "ro" ];
      neededForBoot = true;
    };
    "/var" = {
      fsType = "tmpfs";
      options = [ "mode=0755" ];
    };
  };

  environment.systemPackages = with pkgs; [
    libraspberrypi
    raspberrypi-eeprom
    iw
    libva-utils
    mpv
    vlc
    yt-dlp
    ncpamixer
    pulseaudio # required for pactl
    chromium
    firefox
    pavucontrol
    glxinfo
    projectm
    # tracer-game
    bevy_julia
    bevy_mandelbrot
    allcolors
    ffmpeg
    trainbot
  ];

  nix.gc.automatic = lib.mkForce false;

  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };

  console.keyMap = "de";

  services = {
    # Do not log to flash
    journald.extraConfig = ''
      Storage=volatile
    '';
    openssh.enable = true;
    xserver = {
      enable = true;
      layout = "de";
      xkbOptions = "eurosign:e";
      displayManager = {
        lightdm.enable = true;
        autoLogin = {
          enable = true;
          user = "k-ot";
        };
        defaultSession = "gnome-xorg";
      };
      desktopManager.gnome.enable = true;
    };
  };

  systemd = {
    # r/o /nix/store
    services = {
      nix-daemon.enable = false;
      nix-gc.enable = false;
    };
    sockets.nix-daemon.enable = false;

    user.services.x11vnc = {
      description = "X11 VNC server";
      wantedBy = [ "graphical-session.target" ];
      partOf = [ "graphical-session.target" ];
      serviceConfig = {
        ExecStart = ''
          ${pkgs.x11vnc}/bin/x11vnc -shared -forever -passwd k-ot
        '';
        RestartSec = 3;
        Restart = "always";
      };
    };
  };

  system.stateVersion = "21.11"; # Did you read the comment?
}
