{ lib, ... }:

{
  networking.hostName = "schalter";
  hardware.enableRedistributableFirmware = true;
  #networking.wireless.enable = true;
  powerManagement.cpuFreqGovernor = lib.mkDefault "performance";

  boot = {
    loader = {
      grub.enable = false;
      raspberryPi = {
        enable = true;
        version = 1;
        uboot.enable = false;
      };
      generic-extlinux-compatible.enable = lib.mkForce false;
    };
    # no zfs required
    supportedFilesystems = lib.mkForce [ "vfat" "ext4" ];

    tmpOnTmpfs = true;
  };

  nixpkgs.config.packageOverrides = pkgs: {
    makeModulesClosure = x:
      # prevent kernel install fail due to missing modules
      pkgs.makeModulesClosure (x // { allowMissing = true; });
  };

  sdImage = {
    compressImage = false;
    imageBaseName = "schalter";
    firmwareSize = 512;
  };

  nixpkgs.crossSystem = lib.systems.examples.raspberryPi;
}
