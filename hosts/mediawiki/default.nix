{ config, lib, pkgs, ... }:

let
  cfg = config.services.mediawiki;
in
{
  c3d2.deployment.server = "server10";

  microvm.mem = 1024;

  networking = {
    firewall.allowedTCPPorts = [ 80 443 ]; # httpd, not nginx :(
    hostName = "mediawiki";
  };

  services = {
    backup.paths = [ "/var/lib/mediawiki/uploads/" ];

    logrotate.checkConfig = false;

    mediawiki = {
      enable = true;
      virtualHost = {
        adminAddr = "no-reply@c3d2.de";
        enableACME = true;
        forceSSL = true;
        hostName = "wiki.c3d2.de";
        extraConfig = ''
          RewriteEngine On
          RewriteRule ^/w/(.*).php /$1.php [L,QSA]
          RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} !-f
          RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} !-d
          RewriteCond %{REQUEST_URI} !^/images
          RewriteRule ^(.*) /index.php/$1 [L,QSA]
        '';
      };
      #skins = {
      #      Vector = "${config.services.mediawiki.package}/share/mediawiki/skins/Vector";
      #      Hector = "${config.services.mediawiki.package}/share/mediawiki/skins/Hector";
      #};
      name = "C3D2";

      extraConfig = ''
        $wgArticlePath = '/$1';

        $wgShowExceptionDetails = true;
        $wgDBserver = "${config.services.mediawiki.database.socket}";
        $wgDBmwschema       = "mediawiki";

        $wgLogo =  "https://www.c3d2.de/images/ck.png";
        $wgEmergencyContact = "wiki@c3d2.de";
        $wgPasswordSender   = "wiki@c3d2.de";
        $wgLanguageCode = "de";

        $wgGroupPermissions['*']['edit'] = false;
        $wgGroupPermissions['user']['edit'] = true;
        $wgGroupPermissions['sysop']['interwiki'] = true;
        $wgGroupPermissions['sysop']['userrights'] = true;

        define("NS_INTERN", 100);
        define("NS_INTERN_TALK", 101);

        $wgExtraNamespaces[NS_INTERN] = "Intern";
        $wgExtraNamespaces[NS_INTERN_TALK] = "Intern_Diskussion";

        $wgGroupPermissions['intern']['move']             = true;
        $wgGroupPermissions['intern']['move-subpages']    = true;
        $wgGroupPermissions['intern']['move-rootuserpages'] = true; // can move root userpages
        $wgGroupPermissions['intern']['read']             = true;
        $wgGroupPermissions['intern']['edit']             = true;
        $wgGroupPermissions['intern']['createpage']       = true;
        $wgGroupPermissions['intern']['createtalk']       = true;
        $wgGroupPermissions['intern']['writeapi']         = true;
        $wgGroupPermissions['intern']['upload']           = true;
        $wgGroupPermissions['intern']['reupload']         = true;
        $wgGroupPermissions['intern']['reupload-shared']  = true;
        $wgGroupPermissions['intern']['minoredit']        = true;
        $wgGroupPermissions['intern']['purge']            = true; // can use ?action=purge without clicking "ok"
        $wgGroupPermissions['intern']['sendemail']        = true;

        $wgNamespacePermissionLockdown[NS_INTERN]['*'] = array('intern');
        $wgNamespacePermissionLockdown[NS_INTERN_TALK]['*'] = array('intern');

        define("NS_I4R", 102);
        define("NS_I4R_TALK", 103);
        $wgExtraNamespaces[NS_I4R] = "IT4Refugees";
        $wgExtraNamespaces[NS_I4R_TALK] = "IT4Refugees_Diskussion";
        $wgGroupPermissions['i4r']['move']             = true;
        $wgGroupPermissions['i4r']['move-subpages']    = true;
        $wgGroupPermissions['i4r']['move-rootuserpages'] = true; // can move root userpages
        $wgGroupPermissions['i4r']['read']             = true;
        $wgGroupPermissions['i4r']['edit']             = true;
        $wgGroupPermissions['i4r']['createpage']       = true;
        $wgGroupPermissions['i4r']['createtalk']       = true;
        $wgGroupPermissions['i4r']['writeapi']         = true;
        $wgGroupPermissions['i4r']['upload']           = true;
        $wgGroupPermissions['i4r']['reupload']         = true;
        $wgGroupPermissions['i4r']['reupload-shared']  = true;
        $wgGroupPermissions['i4r']['minoredit']        = true;
        $wgGroupPermissions['i4r']['purge']            = true; // can use ?action=purge without clicking "ok"
        $wgGroupPermissions['i4r']['sendemail']        = true;
        $wgNamespacePermissionLockdown[NS_I4R]['*'] = array('i4r');
        $wgNamespacePermissionLockdown[NS_I4R_TALK]['*'] = array('i4r');

        $wgGroupPermissions['sysop']['deletelogentry'] = true;
        $wgGroupPermissions['sysop']['deleterevision'] = true;

        wfLoadExtension('ConfirmEdit/QuestyCaptcha');
        $wgCaptchaClass = 'QuestyCaptcha';
        $wgCaptchaQuestions[] = array( 'question' => 'How is C3D2 logo in ascii?', 'answer' => '<<</>>' );

        $wgEnableAPI = true;
        $wgAllowUserCss = true;
        $wgUseAjax = true;
        $wgEnableMWSuggest = true;

        //TODO what about $wgUpgradeKey ?

        $wgScribuntoDefaultEngine = 'luastandalone';

        # LDAP
        $LDAPProviderDomainConfigs = "${config.sops.secrets."mediawiki/ldapprovider".path}";
        $wgPluggableAuth_EnableLocalLogin = true;
      '';
      # see https://extdist.wmflabs.org/dist/extensions/ for list of extensions
      # save them on https://web.archive.org/save and copy the final URL below
      extensions = {
        Cite = pkgs.fetchzip {
          url = "https://web.archive.org/web/20230516204128/https://extdist.wmflabs.org/dist/extensions/Cite-REL1_39-2540df4.tar.gz";
          sha256 = "sha256-fXE+W1nRPvMK7fOJa7q0fY3CpT0TrxDUv5R4WKPXxPc=";
        };
        CiteThisPage = pkgs.fetchzip {
          url = "https://web.archive.org/web/20230516204058/https://extdist.wmflabs.org/dist/extensions/CiteThisPage-REL1_39-1c86120.tar.gz";
          sha256 = "sha256-GU3L8rqU9RI7VDK4kcCBLDoBD26Sqk1Bu6hANhlByeQ=";
        };
        ConfirmEdit = pkgs.fetchzip {
          url = "https://web.archive.org/web/20230516203822/https://extdist.wmflabs.org/dist/extensions/ConfirmEdit-REL1_39-09a7ebc.tar.gz";
          sha256 = "sha256-G+ZYmPEva8C9arcpmvREX5yvA12PE3/zjpDpzW6dP9o=";
        };
        Lockdown = pkgs.fetchzip {
          url = "https://web.archive.org/web/20230516203722/https://extdist.wmflabs.org/dist/extensions/Lockdown-REL1_39-12dd618.tar.gz";
          sha256 = "sha256-V4Tdo04YtH6g15QgAW9RPqlVOwMOAyrGGIPbs9jH45A=";
        };
        intersection = pkgs.fetchzip {
          url = "https://web.archive.org/web/20230516203704/https://extdist.wmflabs.org/dist/extensions/intersection-REL1_39-dbb8cfd.tar.gz";
          sha256 = "sha256-E6n+i7+SRHvmSLEIAiUR/LyGFcSkkrwTXl9INa/a4yw=";
        };
        # requires PluggableAuth
        LDAPAuthentication2 = pkgs.fetchzip {
          url = "https://web.archive.org/web/20230516203001/https://extdist.wmflabs.org/dist/extensions/LDAPAuthentication2-REL1_39-35908c0.tar.gz";
          sha256 = "sha256-LWXpmgzUpgEaPe/4cwF2cmJxPkW8ywT7gRAlB58mDfY=";
        };
        LDAPProvider = pkgs.fetchzip {
          url = "https://web.archive.org/web/20230516202850/https://extdist.wmflabs.org/dist/extensions/LDAPProvider-REL1_39-1b79e16.tar.gz";
          sha256 = "sha256-rJGdS1mbmSdHUIgbNeRMJ56vTVihEgXzOvR6k1guDU8=";
        };
        ParserFunctions = pkgs.fetchzip {
          url = "https://web.archive.org/web/20230516202737/https://extdist.wmflabs.org/dist/extensions/ParserFunctions-REL1_39-3eb1eb9.tar.gz";
          sha256 = "sha256-wAoMVNerfa7FUP+NH51cYZf+QKQl+pdSBoKsbAS6LBE=";
        };
        PluggableAuth = pkgs.fetchzip {
          url = "https://web.archive.org/web/20230516202627/https://extdist.wmflabs.org/dist/extensions/PluggableAuth-REL1_39-1210fc3.tar.gz";
          sha256 = "sha256-F6bTMCzkK3kZwZGIsNE87WlZWqXXmTMhEjApO99YKR0=";
        };
        Scribunto = pkgs.fetchzip {
          url = "https://web.archive.org/web/20230516202513/https://extdist.wmflabs.org/dist/extensions/Scribunto-REL1_39-ebb91f2.tar.gz";
          sha256 = "sha256-WHgVyY2JpUp8lFpvtKYS3wNe7UzzYLtwsRqtIdZBhek=";
        };
        WikiEditor = pkgs.fetchzip {
          url = "https://web.archive.org/web/20230516202249/https://extdist.wmflabs.org/dist/extensions/WikiEditor-REL1_39-ed89fa9.tar.gz";
          sha256 = "sha256-Aypjzv0cjoJvPuqSqlvMrlvd8n5EtE4TC8eyxFGwmLQ=";
        };
      };
      passwordFile = config.sops.secrets."mediawiki/adminPassword".path;
      database = {
        type = "postgres";
        socket = "/run/postgresql";
        user = "mediawiki";
        name = "mediawiki";
      };
      uploadsDir = "/var/lib/mediawiki/uploads";
    };

    phpfpm.phpPackage = pkgs.php.buildEnv {
      extensions = { all, enabled }: enabled ++ (with all; [ apcu ]);
    };

    postgresql = {
      enable = true;
      authentication = lib.mkForce ''
        # TYPE  DATABASE        USER            ADDRESS                 METHOD
        local   all             all                                     trust
        host    all             all             127.0.0.1/32            trust
        host    all             all             10.233.2.1/32           trust
        host    all             all             ::1/128                 trust
      '';
      enableTCPIP = true;
      ensureDatabases = [ cfg.database.name ];
      ensureUsers = [{
        name = cfg.database.user;
        ensurePermissions = { "DATABASE ${cfg.database.name}" = "ALL PRIVILEGES"; };
      }];
      package = pkgs.postgresql_15;
      upgrade.stopServices = [ "httpd" "phpfpm-mediawiki" ];
    };
  };

  sops = {
    age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "mediawiki/adminPassword".owner = config.systemd.services.mediawiki-init.serviceConfig.User;
      "mediawiki/ldapprovider".owner = config.systemd.services.mediawiki-init.serviceConfig.User;
      "mediawiki/secretKey" = {
        owner = config.systemd.services.mediawiki-init.serviceConfig.User;
        path = "/var/lib/mediawiki/secret.key";
      };
      "mediawiki/upgradeKey".owner = config.systemd.services.mediawiki-init.serviceConfig.User;
      "restic/password".owner = "root";
      "restic/repository/server8".owner = "root";
    };
  };

  system.stateVersion = "22.05";

  systemd.services.mediawiki-init = {
    after = [ "postgresql.service" ];
    requires = [ "postgresql.service" ];
  };
}
