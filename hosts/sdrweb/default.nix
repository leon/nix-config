{ hostRegistry, pkgs, ... }:
{
  deployment = {
    mem = 4096;
    vcpu = 16;
    needForSpeed = true;
  };
  c3d2.hq.statistics.enable = true;

  system.stateVersion = "22.05";

  networking.hostName = "sdrweb";

  services.nginx = {
    enable = true;
    virtualHosts."sdr.hq.c3d2.de" = {
      default = true;
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "http://localhost:8073";
        proxyWebsockets = true;
      };
    };
    virtualHosts."adsb.hq.c3d2.de" = {
      forceSSL = true;
      enableACME = true;
      root = pkgs.runCommand "adsb-map" {} ''
        mkdir $out
        cp ${./adsb.html} $out/index.html
        cp ${./airplane.svg} $out/airplane.svg
      '';
      extraConfig = ''
          index index.html;
          charset utf-8;
        '';
      locations."/data.json".proxyPass = "http://${hostRegistry.radiobert.ip4}:8080/data.json";
    };
  };

  services.openwebrx.enable = true;

  services.collectd.plugins = {
    curl_json = ''
      <URL "http://localhost:8073/metrics.json">
        Plugin "openwebrx_decodes"
        <Key "wsjt/decodes/*/*/count">
          Type "counter"
        </Key>
        <Key "aprs/decodes/*/*/*/count">
          Type "counter"
        </Key>
        <Key "js8call/decodes/*/*/count">
          Type "counter"
        </Key>
        <Key "decoding/queue/*/count">
          Type "counter"
        </Key>
        <Key "decoding/queue/length">
          Type "gauge"
        </Key>
        <Key "pskreporter/*/count">
          Type "gauge"
        </Key>
        <Key "wsprnet/*/count">
          Type "gauge"
        </Key>
        <Key "openwebrx/users">
          Type "gauge"
        </Key>
      </URL>
    '';
  };

  services.heliwatch = {
    enable = true;
    jid = "astrobot@jabber.c3d2.de";
    inherit (pkgs.mucbot) password;
    muc = "luftraum@chat.c3d2.de/Hubschraubereinsatz";
  };
}
