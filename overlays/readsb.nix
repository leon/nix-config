{ stdenv
, fetchFromGitHub
, pkg-config
, protobufc
, ncurses
, rrdtool
, libusb1
, libbladeRF
, librtlsdr
  # , libad9361, libiio
}:
stdenv.mkDerivation rec {
  pname = "readsb-protobuf";
  version = "unstable-2021-12-10";

  src = fetchFromGitHub {
    owner = "Mictronics";
    repo = pname;
    # branch "dev" as of 2021-12-10
    rev = "b6c1bebc77d80a9366e31c13c73eea95e7a9296a";
    sha256 = "0vg37cngbvahwb8wc6j5i605d8fn4ck0szy13xzw5b3q61aqvaqv";
  };

  nativeBuildInputs = [
    pkg-config
    protobufc
  ];

  buildInputs = [
    ncurses
    rrdtool
    libusb1
    libbladeRF
    librtlsdr
    # libad9361 libiio
  ];

  # enableParallelBuilding = true;

  makeFlags = [
    "BLADERF=yes"
    "RTLSDR=yes"
    # "PLUTOSDR=yes"
  ];

  installPhase = ''
    find . -type f
    mkdir -p $out/bin
    cp -a readsb viewadsb $out/bin/
  '';
}
