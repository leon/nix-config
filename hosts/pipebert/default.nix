{ config, lib, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
  ];

  c3d2 = {
    hq.interface = "eth0";
    hq.statistics.enable = true;
    k-ot.enable = true;
    audioServer.enable = true;
  };

  boot.loader = {
    efi.canTouchEfiVariables = true;
    systemd-boot.enable = true;
  };

  environment.etc."wireplumber/main.lua.d/50-rename.lua".text = /* lua */ ''
    rule = {
      matches = {
        {
          { "node.name", "equals", "alsa_output.usb-0c76_USB_PnP_Audio_Device-00.analog-stereo" },
        },
      },
      apply_properties = {
        ["node.description"] = "Pipebert Audio Streaming",
      },
    }

    table.insert(alsa_monitor.rules,rule)
  '';

  networking = {
    domain = "hq.c3d2.de";
    hostId = "5c4ed15a";
    hostName = "pipebert";
    useDHCP = false;
    interfaces.eth0.useDHCP = true;
  };

  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };

  services = {
    mopidy = {
      enable = true;
      configuration = ''
        [audio]
        output = pulsesink server=127.0.0.1

        [core]
        restore_state = true

        [http]
        allowed_origins =
          cristianpb.github.io
          mopster.urizen.pl

        [youtube]
        allow_cache = true
        youtube_dl_package = yt_dlp
      '';
      extensionPackages = with pkgs; [ mopidy-muse mopidy-youtube python3Packages.yt-dlp ];
    };

    nginx = {
      enable = true;
      virtualHosts = {
        "drkkr.hq.c3d2.de" = {
          default = true;
          enableACME = true;
          forceSSL = true;
          locations."/" = {
            proxyPass = "http://127.0.0.1:${toString config.services.octoprint.port}";
            proxyWebsockets = true;
            extraConfig = ''
              proxy_set_header X-Scheme $scheme;
              proxy_set_header Accept-Encoding identity;
              client_max_body_size 200M;
            '';
          };
          # locations."/cam/stream" = {
          #   proxyPass = "http://localhost:3020/?action=stream";
          #   extraConfig = "proxy_pass_request_headers off;";
          # };
          # locations."/cam/capture" = {
          #   proxyPass = "http://localhost:3020/?action=snapshot";
          #   extraConfig = "proxy_pass_request_headers off;";
          # };
        };
        "drucker.hq.c3d2.de" = {
          enableACME = true;
          forceSSL = true;
          locations."/".return = "307 https://drkkr.hq.c3d2.de/";
        };
        "ledfx.hq.c3d2.de" = {
          enableACME = true;
          forceSSL = true;
          locations."/" = {
            proxyPass = "http://127.0.0.1:8888/";
            proxyWebsockets = true;
          };
        };
        "mopidy.hq.c3d2.de" = {
          enableACME = true;
          forceSSL = true;
          locations."/" = {
            proxyPass = "http://127.0.0.1:6680";
            proxyWebsockets = true;
          };
        };
        "pipebert.hq.c3d2.de" = {
          enableACME = true;
          forceSSL = true;
          locations."/" = {
            extraConfig = ''
              add_header Content-Type text/html;
            '';
            return = ''200 '<!DOCTYPE html>
              <html>
                <body>
                  <ul>
                    <li><a href="https://drkkr.hq.c3d2.de/">Drucker</li></a>
                    <li><a href="https://ledfx.hq.c3d2.de/">LEDfx</li></a>
                    <li><a href="https://mopidy.hq.c3d2.de/muse/">Mopidy</li></a>
                    <li><a href="https://mopidy.hq.c3d2.de/youtube/">Mopidy - Add YouTube link</li></a>
                  </ul>
                </body>
              </html>' '';
          };
        };
      };
    };

    octoprint = {
      enable = true;
      port = 8080;
      # extraConfig.webcam = {
      #   snapshot = "http://localhost:3020?action=snapshot";
      #   stream = "https://drkkr.hq.c3d2.de/cam/stream";
      # };
    };
  };

  system.stateVersion = "22.11";

  users.users = lib.optionalAttrs config.services.octoprint.enable {
    # Allow access to printer serial port and GPIO
    "${config.services.octoprint.user}".extraGroups = [ "dialout" ];
  };
}
