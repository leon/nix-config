{ zentralwerk, config, hostRegistry, pkgs, lib, ... }:

{
  imports = [
    ./proxy.nix
    ./stats.nix
  ];

  c3d2.deployment.server = "server10";

  networking.hostName = "public-access-proxy";

  services.proxy = {
    enable = true;
    proxyHosts = [ {
      hostNames = [ "vps1.nixvita.de" "vps1.codetu.be" "nixvita.de" ];
      proxyTo.host = "172.20.73.51";
      matchArg = "-m end";
    } {
      hostNames = [ "auth.c3d2.de" ];
      proxyTo.host = hostRegistry.auth.ip4;
    } {
      hostNames = [ "jabber.c3d2.de" ];
      proxyTo = {
        host = hostRegistry.jabber.ip4;
      };
      matchArg = "-m end";
    } {
      hostNames = [ "zw.poelzi.org" ];
      proxyTo.host = "172.20.73.162";
      matchArg = "-m end";
    } {
      hostNames = [ "borken.dvb.solutions" "borken.tlm.solutions" ];
      proxyTo = {
        host = hostRegistry.borken-data-hoarder.ip4;
      };
      matchArg = "-m end";
    } {
      hostNames = [ "staging.dvb.solutions" "staging.tlm.solutions" ];
      proxyTo = {
        host = hostRegistry.staging-data-hoarder.ip4;
      };
      matchArg = "-m end";
    } {
      hostNames = [ "dvb.solutions" "tlm.solutions" ];
      proxyTo = {
        host = "172.20.73.69";
      };
      matchArg = "-m end";
    } {
      hostNames = [ "bind.serv.zentralwerk.org" ];
      proxyTo.host = hostRegistry.bind.ip4;
    } {
      hostNames = [ "blogs.c3d2.de" ];
      proxyTo.host = hostRegistry.blogs.ip4;
    } {
      hostNames = [
        "datenspuren.de" "www.datenspuren.de" "ds.c3d2.de" "datenspuren.c3d2.de"
        "c3d2.de" "www.c3d2.de" "c3dd.de" "www.c3dd.de" "cccdd.de" "www.cccdd.de" "dresden.ccc.de" "www.dresden.ccc.de"
        "openpgpkey.c3d2.de"
        "netzbiotop.org" "www.netzbiotop.org"
        "autotopia.c3d2.de"
        "rc3.c3d2.de" "dezentrale-jahresendveranstaltungen.fyi" "www.dezentrale-jahresendveranstaltungen.fyi"
      ];
      proxyTo.host = zentralwerk.lib.config.site.net.flpk.hosts4.c3d2-web;
    } {
      hostNames = [
        "codimd.c3d2.de"
        "hackmd.c3d2.de"
        "hedgedoc.c3d2.de"
      ];
      proxyTo.host = hostRegistry.hedgedoc.ip4;
    } {
      hostNames = [ "ftp.c3d2.de" ];
      proxyTo.host = hostRegistry.ftp.ip4;
    } {
      hostNames = [ "gitea.c3d2.de" ];
      proxyTo.host = hostRegistry.gitea.ip4;
      proxyProtocol = true;
    } {
      hostNames = [ "grafana.hq.c3d2.de" ];
      proxyTo.host = hostRegistry.grafana.ip4;
    } {
      hostNames = [
        "hydra.hq.c3d2.de"
        "hydra-ca.hq.c3d2.de"
        "nix-cache.hq.c3d2.de"
        "nix-serve.hq.c3d2.de"
      ];
      proxyTo.host = hostRegistry.hydra.ip4;
    } {
      hostNames = [
        "zentralwerk.org"
        "www.zentralwerk.org"
      ];
      proxyTo.host = hostRegistry.network-homepage.ip4;
    } {
      hostNames = [ "matemat.hq.c3d2.de" ];
      proxyTo.host = hostRegistry.matemat.ip4;
    } {
      hostNames = [
        "element.c3d2.de"
        "matrix.c3d2.de"
      ];
      proxyTo.host = hostRegistry.matrix.ip4;
    } {
      hostNames = [ "mobilizon.c3d2.de" ];
      proxyTo.host = hostRegistry.mobilizon.ip4;
    } {
      hostNames = [ "drkkr.hq.c3d2.de" ];
      proxyTo.host = hostRegistry.pulsebert.ip4;
    } {
      hostNames = [ "scrape.hq.c3d2.de" ];
      proxyTo.host = hostRegistry.scrape.ip4;
    } {
      hostNames = [
        "adsb.hq.c3d2.de"
        "sdr.hq.c3d2.de"
      ];
      proxyTo.host = hostRegistry.sdrweb.ip4;
    } {
      hostNames = [
        "stream.hq.c3d2.de" "torrents.hq.c3d2.de"
      ];
      proxyTo.host = hostRegistry.stream.ip4;
    } {
      hostNames = [ "ticker.c3d2.de" ];
      proxyTo.host = hostRegistry.ticker.ip4;
    } {
      hostNames = [ "wiki.c3d2.de" ];
      proxyTo.host = hostRegistry.mediawiki.ip4;
    } {
      hostNames = [ "owncast.c3d2.de" ];
      proxyTo.host = hostRegistry.owncast.ip4;
    } {
      hostNames = [ "c3d2.social" ];
      proxyTo.host = hostRegistry.mastodon.ip4;
    } {
      hostNames = [ "relay.fedi.buzz" ];
      proxyTo.host = zentralwerk.lib.config.site.net.serv.hosts4.buzzrelay;
    } {
      hostNames = [ "tmppleroma.hq.c3d2.de" ];
      proxyTo.host = zentralwerk.lib.config.site.net.serv.hosts4.tmppleroma;
    } {
      hostNames = [ "oxigraph.hq.c3d2.de" ];
      proxyTo.host = zentralwerk.lib.config.site.net.serv.hosts4.oxigraph;
    } {
      hostNames = [ "drone.hq.c3d2.de" ];
      proxyTo.host = hostRegistry.drone.ip4;
      proxyProtocol = true;
    } {
      hostNames = [ "home-assistant.hq.c3d2.de" ];
      proxyTo.host = hostRegistry.home-assistant.ip4;
    } ];
  };

  networking.firewall.allowedTCPPorts = [
    # haproxy
    80 443
    # gemini
    1965
  ];

  # DNS records IN AAAA {www.,}c3d2.de point to this host but
  # gemini:// is served on c3d2-web only
  systemd.services.gemini-forward = {
    wantedBy = [ "multi-user.target" ];
    path = with pkgs; [ socat ];
    script = ''
      socat tcp6-listen:1965,fork "tcp6:[${zentralwerk.lib.config.site.net.flpk.hosts6.flpk.c3d2-web}]:1965"
    '';
    serviceConfig = {
      ProtectSystem = "strict";
      DynamicUser = true;
    };
  };

  system.stateVersion = "18.09";
}
