{ config, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./microvm-staging.nix
    ./znapzend.nix
  ];

  c3d2 = {
    baremetal = true;
    deployment.microvmBaseZfsDataset = "server10/vm";
    hq.statistics.enable = true;
  };

  boot = {
    loader.grub = {
      enable = true;
      version = 2;
      device = "/dev/sda";
    };
    kernelParams = [
      "preempt=none"
      # No server/router runs any untrusted user code
      "mitigations=off"
    ];
    tmpOnTmpfs = true;
    tmpOnTmpfsSize = "80%";
  };

  networking = {
    firewall = {
      enable = true;
      allowedTCPPorts = [ 22 ];
    };
    hostName = "server10";
    # TODO: change that to something more random
    hostId = "10101010";
  };

  # reserve resources for legacy MicroVMs
  services.nomad.settings.client.reserved = {
    cpu = 4200;
    # see /sys/fs/cgroup/system.slice/system-microvm.slice/memory.current
    memory = 28 * 1024;
  };

  simd.arch = "ivybridge";

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets."machine-id" = {
      mode = "444";
      path = "/etc/machine-id";
    };
    secrets."ceph/osd.4/keyfile" = {};
  };

  # static list of microvms from other sources
  microvm.autostart = [
    "data-hoarder"
    "staging-data-hoarder"
    "borken-data-hoarder"
    "tram-borzoi"
  ];
  skyflake.nomad.client.meta."c3d2.cpuSpeed" = "4";
  skyflake.storage.ceph.osds = [ {
    id = 4;
    fsid = "21ff9a57-c8d1-4cfa-8e01-c09ae0c2f0e3";
    path = "/dev/zvol/server10/ceph-osd.4";
    keyfile = config.sops.secrets."ceph/osd.4/keyfile".path;
    deviceClass = "ssd";
  } ];

  system.stateVersion = "21.11"; # Did you read the comment?
}
