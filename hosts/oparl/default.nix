{ oparl-scraper, config, pkgs, ... }:

let
  ratsinfo-scraper = import oparl-scraper { inherit pkgs; };
in
{
  c3d2.deployment.server = "server10";
  microvm.mem = 1024;

  networking.hostName = "oparl";

  users.users.oparl = {
    isNormalUser = true;
    createHome = true;
    group = "oparl";
  };
  users.groups.oparl = {};
  systemd.tmpfiles.rules = [
    "C ${config.users.users.oparl.home}/.ssh/id_ed25519 0400 oparl oparl - ${config.sops.secrets."users/oparl/id_ed25519".path}"
    "z ${config.users.users.oparl.home}/.ssh/id_ed25519 0400 oparl oparl - -"
  ];

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets."users/oparl/id_ed25519" = {};
  };

  systemd.services.oparl-scraper = {
    wantedBy = [ "multi-user.target" ];
    after = [ "systemd-tmpfiles-setup.service" "network-online.target" ];
    startAt = "daily";
    serviceConfig = {
      User = "oparl";
      Group = "oparl";
      WorkingDirectory = "${config.users.users.oparl.home}";
    };
    path = with pkgs; [
      git openssh poppler_utils ratsinfo-scraper
    ];
    script = /* bash */ ''
      if [ -d data ]; then
        pushd data
        git pull
        popd
      else
        git clone git@github.com:offenesdresden/dresden-ratsinfo.git data
      fi

      pushd data
      git config user.name Astro
      git config user.email astro@spaceboyz.net
      popd

      scrape
      pushd data
      find . -name \*.json -exec git add "{}" +
      git commit -m update || true
      git push origin master
      popd

      meetings2ics data/meetings/ > data/meetings.ics
      pushd data
      git add meetings.ics
      git commit -m "update meetings.ics" || true
      git push origin master
      popd

      files_extract
      pushd data
      find . -name \*.txt -exec git add {} +
      git commit -m update\ text || true
      git push origin master
    '';
  };

  system.stateVersion = "22.05";
}
