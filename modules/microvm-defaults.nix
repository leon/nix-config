# No MicroVM settings but some defaults that enable evaulating NixOS
# configurations that are destined to be used on Skyflake

{ config, lib, ... }:

{
  # autoupdates do not make sense inside MicroVMs with read-only /nix/store
  c3d2.autoUpdate = false;

  boot = {
    loader.grub.enable = false;
    kernel.sysctl = lib.optionalAttrs (config.microvm.mem <= 1024) {
      # table overflow causing packets from nginx to the service to drop
      # nf_conntrack: nf_conntrack: table full, dropping packet
      "net.netfilter.nf_conntrack_max" = "65536";
    };
    kernelParams = [
      "preempt=none"
      # No server/router runs any untrusted user code
      "mitigations=off"
    ];
  };

  fileSystems."/" = lib.mkDefault {
    fsType = "tmpfs";
  };

  hardware.enableRedistributableFirmware = false;

  # required that sysctl contains net.netfilter.nf_conntrack_max on boot
  networking.firewall.autoLoadConntrackHelpers = true;

  # nix store is mounted read only
  nix.gc.automatic = false;

  systemd.tmpfiles.rules = [
    "d /home/root 0700 root root -" # createHome does not create it
  ];

  users = {
    mutableUsers = false;
    # store root users files persistent, especially .bash_history
    users."root" = {
      createHome = true;
      home = lib.mkForce "/home/root";
    };
  };
}
