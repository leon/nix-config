# Quick full backups of all service MicroVM datasets.
# server10 runs services, server8+9 have the storage.
{
  services.znapzend = {
    enable = true;
    logLevel = "info";
    autoCreation = true;
    # override preexisting zetups
    pure = true;
    zetup = {
      "server10/vm" = {
        recursive = true;
        # keep a day of hourly snapshots locally on server10
        plan = "24h => 1h";
        destinations = {
          server8 = rec {
            dataset = "server8_hdd/backups/server10/vm";
            host = "server8.cluster.zentralwerk.org";
            plan = "2h => 1h, 7d => 24h";
            # just always work
            presend = "ssh-keygen -F ${host} >/dev/null || ssh-keyscan ${host} >> .ssh/known_hosts";
          };
        };
      };
    };
  };
}
