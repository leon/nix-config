{ config, lib, pkgs, ... }:

{
  c3d2.deployment.server = "server10";

  microvm.mem = 2 * 1024;

  networking.hostName = "matemat";

  services = {
    nginx = {
      enable = true;
      virtualHosts."matemat.hq.c3d2.de" = {
        default = true;
        forceSSL = true;
        enableACME = true;
        locations."/" = {
          proxyPass = "http://localhost:3000";
          # ip ranges duplicated with prometheus node exporter
          extraConfig = ''
            satisfy any;
            auth_basic secured;
            auth_basic_user_file ${config.sops.secrets."nginx/basic-auth".path};
            allow 2a00:8180:2c00:200::/56;
            allow 2a0f:5382:acab:1400::/56;
            allow fd23:42:c3d2:500::/56;
            allow 30c:c3d2:b946:76d0::/64;
            allow 172.22.99.0/24;
            allow 172.20.72.0/21;
            deny all;
          '';
        };
      };
    };

    postgresql = {
      package = pkgs.postgresql_15;
      upgrade.stopServices = [ "yammat" ];
    };

    yammat = {
      enable = true;
      config = lib.mkOptionDefault ''
        copyright_link: "https://gitea.c3d2.de/c3d2/yammat"
      '';
    };
  };

  programs.msmtp = {
    enable = true;
    accounts.default = {
      host = "mail.c3d2.de";
      port = 587;
      tls = true;
      tls_starttls = true;
      auth = false;
      domain = "matemat.hq.c3d2.de";
      from = "nek0@c3d2.de";
    };
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "nginx/basic-auth".owner = "nginx";
      "restic/password".owner = "root";
      "restic/repository/server8".owner = "root";
    };
  };

  system.stateVersion = "22.05";
}
