{ lib
, stdenv
, buildGoModule
, fetchFromGitHub
, ffmpeg
}:

buildGoModule {
  pname = "trainbot";
  version = "unstable-2023-05-07";

  src = fetchFromGitHub {
    owner = "jo-m";
    repo = "trainbot";
    rev = "82444a14cba5f611c620f752e79d8bf5e3c5b416";
    sha256 = "sha256-4f5TtTxsJyfT/N9wElnAYxUTuPmx90zQN9afA0UylCU=";
  };

  checkInputs = [ ffmpeg ];
  doCheck = false;

  vendorHash = "sha256-DphXCfPW4w0aGI1e3aKQ9pDAMJ8wioPCDqRUR5gJ+Q4=";
}
