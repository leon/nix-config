{ lib, pkgs, ssh-public-keys, ... }:

{
  deployment = {
    persistedShares = [ "/etc" "/home" "/var" ];
    mem = 2048;
  };

  nix.settings.auto-optimise-store = lib.mkForce false;

  networking = {
    hostName = "leoncloud";
    firewall.enable = true;
  };

  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };
  
  c3d2.hq.statistics.enable = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    wget
    vim
    python3Full
    python310Packages.pip
    python310Packages.flask
    nmap
    htop
    wireguard-tools
    docker-compose 
  ];

  users.users.overdose = {
    isNormalUser = true;
    extraGroups = [ "wheel" "docker" ];
    createHome = true;
    openssh.authorizedKeys.keys = ssh-public-keys.leon;
};


# enable IP routing

  networking.firewall = {
    allowedTCPPorts = [ 80 443 8080 22 53 14000 14500 15000  ];
    allowedUDPPorts = [ 18900 53 ];  
  };

#>-----------------docker-------------------------

  virtualisation.docker.enable = true;

#<-----------------docker-------------------------

#>-----------------wireguard client---------------

# Enable WireGuard
  networking.wireguard.interfaces = {
    vpn = {
      ips = [ "10.10.11.4/24" ];
      privateKeyFile = "/etc/nixos/wireguard-keys/private-key";
      peers = [
        {
          publicKey = "iEVq4lvvKFfqjcoYYyNkA0MS8rcSGaDfPwQGN3C7+D0=";
          allowedIPs = [ "10.10.11.0/24" ];
          endpoint = "45.158.40.162:18900";
          persistentKeepalive = 25;
        }
      ];
    };
  };

#<-----------------wireguard client---------------
#>-----------------nextcloud----------------------

services.nextcloud = {
    enable = true;
    hostName = "cloud";
    config = {
      dbtype = "pgsql";
      dbuser = "nextcloud";
      dbhost = "/run/postgresql"; # nextcloud will add /.s.PGSQL.5432 by itself
      dbname = "nextcloud";
      adminpassFile = "/etc/nixos/next-cloud/pass";
      adminuser = "root";
      extraTrustedDomains = ["10.10.11.4" "10.10.11.1" "45.158.40.165" "bicospacetech.cloud.c3d2.de"]; 
   };
  };

  services.backup.enable = false;

  services.postgresql = {
    enable = true;
    ensureDatabases = [ "nextcloud" ];
    ensureUsers = [
      {
        name = "nextcloud";
        ensurePermissions."DATABASE nextcloud" = "ALL PRIVILEGES";
      }
    ];
  };

  # ensure that postgres is running *before* running the setup
  systemd.services."nextcloud-setup" = {
    requires = ["postgresql.service"];
    after = ["postgresql.service"];
  };



#<-----------------nextcloud----------------------

  system.stateVersion = "22.05";

}
