{ pkgs, ... }:
{
  deployment.mem = 2048;

  networking = {
    hostName = "tmppleroma";
    firewall.allowedTCPPorts = [
      80 443
    ];
  };

  environment.systemPackages = with pkgs; [ pleroma-otp ];

  services.postgresql = {
    enable = true;
    package = pkgs.postgresql_15;
  };

  services.pleroma = {
    enable = true;
    secretConfigFile = "/var/lib/pleroma/secrets.exs";
    configs = [
      ''
        import Config

        config :pleroma, Pleroma.Web.Endpoint,
           url: [host: "tmppleroma.hq.c3d2.de", scheme: "https", port: 443],
           http: [ip: {127, 0, 0, 1}, port: 4000],
           secret_key_base: "lcOBbHZPbGMkpfifPgn5UwUNy0twrSnZplGYceWQ6JZtG7vaUu0QpKy/vGkBVi2o",
           signing_salt: "Li+Voq8h"

        config :pleroma, :instance,
          name: "Temporary Pleroma Instance",
          email: "astro@c3d2.de",
          notify_email: "astro@c3d2.de",
          limit: 5000,
          registrations_open: true

        config :pleroma, :media_proxy,
          enabled: false,
          redirect_on_failure: true
          #base_url: "https://cache.pleroma.social"

        config :pleroma, Pleroma.Repo,
          adapter: Ecto.Adapters.Postgres,
          username: "pleroma",
          password: "ZSfzzg93MGLmEBrkKY3H//k2nhWTJZq4IBi/mDaIU9HcGE2gXdLLfSnak+Y5mpyj",
          database: "pleroma",
          hostname: "localhost"

        # Configure web push notifications
        config :web_push_encryption, :vapid_details,
          subject: "mailto:astro@c3d2.de",
          public_key: "BIRoExJLLKr8qu5CjOcbEvv55DsxvcQrVhCmWKtKoYVi6uZRp6dL7V4_9zdyouolg60wKERt1wFaLr8v3BuZckE",
          private_key: "hch8xAe2KkkpvXKTC6ybwxGJqhfFUPMFNU1ags5dgWU"

        config :pleroma, :database, rum_enabled: false
        config :pleroma, :instance, static_dir: "/var/lib/pleroma/static"
        config :pleroma, Pleroma.Uploaders.Local, uploads: "/var/lib/pleroma/uploads"

        # Enable Strict-Transport-Security once SSL is working:
        # config :pleroma, :http_security,
        #   sts: true

        # Configure S3 support if desired.
        # The public S3 endpoint (base_url) is different depending on region and provider,
        # consult your S3 provider's documentation for details on what to use.
        #
        # config :pleroma, Pleroma.Upload,
        #  uploader: Pleroma.Uploaders.S3,
        #  base_url: "https://s3.amazonaws.com"
        #
        # config :pleroma, Pleroma.Uploaders.S3,
        #   bucket: "some-bucket",
        #   bucket_namespace: "my-namespace",
        #   truncated_namespace: nil,
        #   streaming_enabled: true
        #
        # Configure S3 credentials:
        # config :ex_aws, :s3,
        #   access_key_id: "xxxxxxxxxxxxx",
        #   secret_access_key: "yyyyyyyyyyyy",
        #   region: "us-east-1",
        #   scheme: "https://"
        #
        # For using third-party S3 clones like wasabi, also do:
        # config :ex_aws, :s3,
        #   host: "s3.wasabisys.com"

        config :joken, default_signer: "u3DwPUxgU1n2v5DQT6lBt1p1hzq1E1YfIFUoADArzY2ZGRMt1trctw5tfAa9HmNn"

        config :pleroma, configurable_from_database: true
      ''
    ];
  };

  services.nginx = {
    enable = true;
    virtualHosts."tmppleroma.hq.c3d2.de" = {
      default = true;
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "http://127.0.0.1:4000";
      };
    };
  };
}
