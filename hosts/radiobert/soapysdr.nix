{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    soapysdr-with-plugins
    hackrf
  ];

  users = {
    groups.soapysdr = {};
    users.soapysdr = {
      isSystemUser = true;
      group = "soapysdr";
    };
  };

  systemd.services.soapysdr-server = {
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      ExecStart = "${pkgs.soapysdr-with-plugins}/bin/SoapySDRServer --bind";
      User = "soapysdr";
      Group = "soapysdr";
      ProtectSystem = "full";
      ProtectHome = true;
      Restart = "always";
      RestartSec = "60s";
    };
  };
}
