{ config, ... }:

{
  imports = [
    ./hardware-configuration.nix
  ];

  c3d2 = {
    baremetal = true;
    # deployment.microvmBaseZfsDataset = "tank/storage";
    hq.statistics.enable = true;
  };

  boot = {
    loader.grub = {
      enable = true;
      version = 2;
      # Define on which hard drive you want to install Grub.
      device = "/dev/disk/by-id/scsi-3600300570140a6102b0acad9825149f2"; # or "nodev" for efi only
    };
    kernelParams = [
      "preempt=none"
      # No server/router runs any untrusted user code
      "mitigations=off"
    ];
    tmpOnTmpfs = true;
    tmpOnTmpfsSize = "80%";
  };

  networking = {
    hostName = "server8";
    hostId = "08080808";
  };

  services = {
    nginx = {
      enable = true;
      virtualHosts."server8.cluster.zentralwerk.org" = {
        default = true;
        forceSSL = true;
        enableACME = true;
        locations."/restic/" = {
          proxyPass = "http://${config.services.restic.server.listenAddress}/";
          extraConfig = ''
            client_max_body_size 40M;
            proxy_buffering off;
          '';
        };
      };
    };

    restic.server = {
      enable = true;
      listenAddress = "127.0.0.1:8080";
      privateRepos = true;
    };
  };

  simd.arch = "westmere";

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "ceph/osd.1/keyfile" = {};
      "ceph/osd.2/keyfile" = {};
      "machine-id" = {
        mode = "444";
        path = "/etc/machine-id";
      };
      "restic/htpasswd" = {
        group = config.systemd.services.restic-rest-server.serviceConfig.Group;
        mode = "400";
        owner = config.systemd.services.restic-rest-server.serviceConfig.User;
        path = "/var/lib/restic/.htpasswd";
      };
    };
  };

  skyflake.nomad.client.meta."c3d2.cpuSpeed" = "3";
  skyflake.storage.ceph.osds = [ {
    id = 1;
    fsid = "4b196252-efb6-4ad2-9e9b-cc3fcd664a3a";
    path = "/dev/zvol/server8_root/ceph-osd.1";
    keyfile = config.sops.secrets."ceph/osd.1/keyfile".path;
    deviceClass = "ssd";
  } {
    id = 2;
    fsid = "b860ec59-3314-4fd1-be45-35a46fd8c059";
    path = "/dev/zvol/server8_hdd/ceph-osd.2";
    keyfile = config.sops.secrets."ceph/osd.2/keyfile".path;
    deviceClass = "hdd";
  } ];

  system.stateVersion = "22.11";
}
