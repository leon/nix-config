{ config, libS, pkgs, ... }:

{
  c3d2.deployment.server = "server10";
  microvm = {
    mem = 1 * 1024;
    vcpu = 2;
  };

  networking.hostName = "matrix";

  services = {
    backup.paths = [ "/var/lib/matrix-synapse/" ];

    matrix-synapse = {
      enable = true;
      element-web = {
        enable = true;
        domain = "element.c3d2.de";
      };
      extraConfigFiles = [
        config.sops.secrets."matrix-synapse/config".path
      ];
      ldap = {
        enable = true;
        bindPasswordFile = config.sops.secrets."matrix-synapse/ldapSearchUserPassword".path;
        userFilter = config.security.ldap.groupFilter "matrix";
      };
      settings = {
        admin_contact = "mailto:mail@c3d2.de";
        email = {
          enable_notifs = true;
          notif_for_new_users = false;
          notif_from = "Your Friendly %(app)s homeserver <matrix@c3d2.de>";
          require_transport_security = true;
          smtp_host = "mail.c3d2.de";
          smtp_user = "matrix@c3d2.de";
        };
        enable_registration = false;
        # duplicated in extraConfigFile since synapse is not deep merging the files
        # password_config = {
        #   policy = {
        #     enabled = true;
        #     require_digit = true;
        #     require_lowercase = true;
        #     require_symbol = true;
        #     require_uppercase = true;
        #   };
        # };
        public_baseurl = "https://matrix.c3d2.de/";
        registration_requires_token = true;
        report_stats = false;
        retention = {
          enabled = true;
          default_policy = {
            min_lifetime = "1d";
            max_lifetime = "1y";
          };
        };
        server_name = "c3d2.de";
        serve_server_wellknown = true;
        url_preview_enabled = true;
        user_ips_max_age = "7d";
      };
    };

    matterbridge = {
      enable = true;
      configPath = config.sops.secrets."matterbridge/config".path;
    };

    nginx = {
      enable = true;
      virtualHosts."matrix.c3d2.de" = {
        forceSSL = true;
        enableACME = true;
        locations = {
          "/".proxyPass = "http://127.0.0.1:8008";
          "^~ /_synapse/admin/".return = "403";
        };
      };
    };

    portunus.addToHosts = true;

    postgresql = {
      enable = true;
      ensureUsers = [{
        name = "matrix-synapse";
      }];
      # TODO: move into nixos-modules?
      initialScript = pkgs.writeText "synapse-init.sql" ''
        CREATE ROLE "matrix-synapse" WITH LOGIN;
        CREATE DATABASE "matrix-synapse" WITH OWNER "matrix-synapse"
          TEMPLATE template0
          LC_COLLATE = "C"
          LC_CTYPE = "C";
      '';
      package = pkgs.postgresql_15;
      upgrade.stopServices = [ "matrix-synapse" ];
    };
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = with libS.sops; {
      "matterbridge/config" = permissionForUser "matterbridge";
      "matrix-synapse/config" = permissionForUser "matrix-synapse";
      "matrix-synapse/ldapSearchUserPassword" = permissionForUser "matrix-synapse";
      "restic/password".owner = "root";
      "restic/repository/server8".owner = "root";
    };
  };

  system.stateVersion = "22.11";
}
