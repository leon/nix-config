{
  gis-distance = {
    groups = [ "default" ];
    platforms = [ ];
    source = {
      remotes = [ "https://rubygems.org" ];
      sha256 = "1kgv1scv25b65d9xfricj1ayd2iry7imgk7qw4mryd91mhriibaf";
      type = "gem";
    };
    version = "1.1.0";
  };
  influxdb = {
    groups = [ "default" ];
    platforms = [ ];
    source = {
      remotes = [ "https://rubygems.org" ];
      sha256 = "1l2sjf8kaw3adjjg3l7zg1j735yxdfldf04gl9kjc3hbpdcd7d4w";
      type = "gem";
    };
    version = "0.8.1";
  };
}
