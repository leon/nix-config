{ config, pkgs, ... }:
{
  c3d2 = {
    deployment.server = "server10";
    hq.statistics.enable = true;
  };

  microvm = {
    mem = 1024;
    vcpu = 8;
  };

  networking.hostName = "buzzrelay";
  # Don't let journald spam the disk
  services.journald.extraConfig = ''
    Storage=volatile
  '';

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "buzzrelay/privKey".owner = config.services.buzzrelay.user;
      "buzzrelay/pubKey".owner = config.services.buzzrelay.user;
      "restic/password".owner = "root";
      "restic/repository/server8".owner = "root";
    };
  };

  services = {
    buzzrelay = {
      enable = true;
      hostName = "relay.fedi.buzz";
      privKeyFile = config.sops.secrets."buzzrelay/privKey".path;
      pubKeyFile = config.sops.secrets."buzzrelay/pubKey".path;
    };

    nginx = {
      enable = true;
      virtualHosts."relay.fedi.buzz" = {
        forceSSL = true;
        enableACME = true;
        locations."/".proxyPass = "http://127.0.0.1:${toString config.services.buzzrelay.listenPort}/";
      };
    };

    postgresql = {
      package = pkgs.postgresql_15;
      settings.log_min_duration_statement = 50;
      upgrade.stopServices = [ "buzzrelay" ];
      ensureUsers = [ {
        name = "collectd";
        ensurePermissions."DATABASE ${config.services.buzzrelay.database}" = "ALL PRIVILEGES";
      } ];
    };

    collectd.plugins.postgresql = ''
      <Query unique_followers>
        Statement "select count(distinct id) from follows;"
        <Result>
          Type gauge
          InstancePrefix "unique"
          ValuesFrom "count"
        </Result>
      </Query>
      <Query total_follows>
        Statement "select count(id) from follows;"
        <Result>
          Type gauge
          InstancePrefix "total"
          ValuesFrom "count"
        </Result>
      </Query>

      <Database ${config.networking.hostName}>
        Param database "${config.services.buzzrelay.database}"
        Query unique_followers
        Query total_follows
      </Database>
    '';
  };

  system.stateVersion = "22.11";
}
