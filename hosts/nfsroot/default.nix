{ lib, ... }:
let
  nfsExports = [
    "var/lib/nfsroot/dacbert"
    "var/lib/nfsroot/riscbert"
    "var/lib/dump-dvb/whoopsie"
  ];
in {
  imports = [
    ./tftp.nix
    ./nfs.nix
  ];

  microvm = {
    hypervisor = "cloud-hypervisor";
    mem = 2048;

    # shares break nfs
    shares = lib.mkForce [];
    bootDiskType = "erofs";

    volumes = map (export: {
      mountPoint = "/${export}";
      image = "/dev/zvol/server10/vm/nfsroot/${builtins.baseNameOf export}";
      autoCreate = false;
    }) nfsExports;
  };

  c3d2.deployment.server = "server10";

  fileSystems = builtins.foldl' (fileSystems: export: fileSystems // {
    "/${export}".options = [ "relatime" "discard" ];
  }) {} nfsExports;

  networking = {
    hostName = "nfsroot";

    firewall.enable = false;
  };

  system.stateVersion = "22.05";
}
