{ lib, zentralwerk }:

rec {
  extractZwHosts = { hosts4, hosts6, ... }:
    lib.recursiveUpdate (
      builtins.foldl' (result: name:
        lib.recursiveUpdate result {
          "${name}".ip4 = hosts4."${name}";
        }
      ) {} (builtins.attrNames hosts4)
    ) (
      builtins.foldl' (result: ctx:
        builtins.foldl' (result: name:
          lib.recursiveUpdate result {
            "${name}".ip6 = hosts6."${ctx}"."${name}";
          }
        ) result (builtins.attrNames hosts6."${ctx}")
      ) {} (builtins.attrNames hosts6)
    );

  hostRegistry =
    builtins.foldl' (result: net:
      lib.recursiveUpdate result (extractZwHosts zentralwerk.lib.config.site.net."${net}")
    ) {} [ "core" "cluster" "c3d2" "serv" "flpk" "pub" ];
}
