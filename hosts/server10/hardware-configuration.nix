{ config, lib, modulesPath, ... }:

{
  imports =
    [ (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.initrd.availableKernelModules = [ "uhci_hcd" "ehci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];
  boot.supportedFilesystems = [ "ext2" "zfs" ];
  boot.zfs.devNodes = "/dev/";

  fileSystems."/" =
    { device = "server10/nixos";
      fsType = "zfs";
    };

  fileSystems."/nix" =
    { device = "server10/nixos/nix";
      fsType = "zfs";
    };

  fileSystems."/nix/var" =
    { device = "server10/nixos/nix/var";
      fsType = "zfs";
    };

  fileSystems."/nix/store" =
    { device = "server10/nixos/nix/store";
      fsType = "zfs";
    };

  fileSystems."/var" =
    { device = "server10/nixos/var";
      fsType = "zfs";
    };

  fileSystems."/home" =
    { device = "server10/home";
      fsType = "zfs";
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/b13a876b-2488-47a3-b9bd-3b03fbac6c85";
      fsType = "ext2";
    };

  swapDevices = [ ];

  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
