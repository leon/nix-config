{ pkgs, tigger, ... }:

{
  deployment = {
    # needs to keep just its ssh key for sops-nix
    persistedShares = [ "/etc" "/var" ];
  };

  networking.hostName = "mucbot";

  users.users.tigger = {
    createHome = true;
    isNormalUser = true;
    group = "tigger";
  };
  users.groups.tigger = { };
  services.tigger = {
    enable = true;
    user = "tigger";
    group = "tigger";
    jid = "astrobot@jabber.c3d2.de";
    inherit (pkgs.mucbot) password;
    mucs = [ "c3d2@chat.c3d2.de/Astrobot" "international@chat.c3d2.de/Astrobot" ];
  };

  system.stateVersion = "18.09";
}
