_:

{
  defaultListen = let
    listen = [
      {
        addr = "[::]";
        port = 80;
      }
      {
        addr = "[::]";
        port = 443;
        ssl = true;
      }
      {
        addr = "[::]";
        port = 81;
        extraParameters = [ "proxy_protocol" ];
      }
      {
        addr = "[::]";
        port = 444;
        ssl = true;
        extraParameters = [ "proxy_protocol" ];
      }
    ];
  in
  map (x: (x // { addr = "0.0.0.0"; })) listen ++ listen;
}
