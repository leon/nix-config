# Design

We are using [portunus](https://github.com/majewsky/portunus) to manage an OpenLDAP server
and currently [dex](https://dexidp.io/) to offer OIDC.
Dex might be replaced in the future with an equivalent solution that can remember sessions to have true SSO.
New services should use OAuth/OIDC if possible to lay the groundwork for SSO.
If the application only support LDAP, that is also fine to use.

# How to use it

See the grafana configuration to see an example on how to use OAuth.
To create a new application edit the dex configuration next to portunus.
The aplication credentials are saved in sops.

For an exmaple ldap configuration see the gitea, hydra or mailtngbert.
The ldap settings are documented in portunus in detail.
To connect to `auth.c3d2.de` the nixos-modules option `services.portunus.addToHosts` should be set to true.
