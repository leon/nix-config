{ config, pkgs, ... }:
{
  c3d2.deployment.server = "server10";

  microvm.mem = 2048;

  networking.hostName = "mobilizon";

  services.mobilizon = {
    enable = true;
    settings.":mobilizon".":instance" = {
      name = "C3D2 Mobilizon";
      hostname = "mobilizon.c3d2.de";
      registrations_open = true;
      default_language = "de";
    };
    settings.":mobilizon"."Mobilizon.Web.Email.Mailer" = {
      adapter = { value = "Bamboo.SMTPAdapter"; _elixirType = "raw"; };
      server = "mail.c3d2.de";
      hostname = config.networking.hostName;
      auth = false;
      port = 587;
      ssl = false;
      tls = { value = ":if_available"; _elixirType = "atom"; };
      allowed_tls_versions = { value = ''[:tlsv1, :"tlsv1.1", :"tlsv1.2"]''; _elixirType = "raw"; };
      retries = 1;
      no_mx_lookups = true;
    };
    settings.":mobilizon".":logger" = {
      level = { value = ":all"; _elixirType = "atom"; };
    };
  };

  services.nginx = {
    enable = true;
    virtualHosts."mobilizon.c3d2.de" = {
      default = true;
      forceSSL = true;
      enableACME = true;
    };
  };

  services.postgresql = {
    extraPlugins = with config.services.postgresql.package.pkgs; [ postgis ];
    package = pkgs.postgresql_15;
    upgrade.stopServices = [ "mobilizon" ];
  };

  system.stateVersion = "22.05";
}
