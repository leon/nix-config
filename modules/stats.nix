{ config, lib, pkgs, ... }:

let
  cfg = config.c3d2.hq.statistics;

  isMetal =
    !config.boot.isContainer &&
    !(config ? microvm);

in
{
  options.c3d2.hq.statistics = {
    enable = lib.mkEnableOption "statistics collection";
  };

  config = {
    services = lib.mkMerge [
      (let
        nginxStatusPort = 9100;
      in {
        collectd = lib.mkIf cfg.enable {
          enable = true;
          extraConfig = ''
            FQDNLookup false
            Interval 10
          '';
          buildMinimalPackage = true;
          plugins = {
            logfile = ''
              LogLevel info
              File STDOUT
            '';
            network = ''
              Server "grafana.serv.zentralwerk.org" "25826"
            '';
            memory = "";
            processes = "";
            disk = "";
            df = "";
            cpu = "";
            entropy = "";
            load = "";
            swap = "";
            cgroups = "";
            vmem = "";
            interface = "";
          } // lib.optionalAttrs isMetal {
            sensors = "";
            cpufreq = "";
            irq = "";
            ipmi = "";
            thermal = "";
          } // lib.optionalAttrs config.services.nginx.enable {
            nginx = ''
              URL "http://localhost:${toString nginxStatusPort}/nginx_status"
            '';
          };
        };

        nginx = lib.mkIf config.services.nginx.enable {
          virtualHosts.localhost = {
            listen = [
              { addr = "127.0.0.1"; port = nginxStatusPort; }
              { addr = "[::1]"; port = nginxStatusPort; }
            ];
            locations."/nginx_status".extraConfig = ''
              stub_status;

              access_log off;
              allow 127.0.0.1;
              allow ::1;
              deny all;
            '';
          };
        };
      })

      (lib.mkIf (pkgs.system != "riscv64-linux") {
        nginx = {
          enable = true;
          virtualHosts."_" = {
            listen =
              let
                port = 9100;
              in
              [
                { addr = "0.0.0.0"; inherit port; }
                { addr = "[::]"; inherit port; }
              ];
            locations."/metrics" = {
              proxyPass = "http://127.0.0.1:${toString config.services.prometheus.exporters.node.port}/metrics";
              # ip ranges duplicated with matemat
              extraConfig = ''
                satisfy any;
                allow 2a00:8180:2c00:200::/56;
                allow 2a0f:5382:acab:1400::/56;
                allow fd23:42:c3d2:500::/56;
                allow 30c:c3d2:b946:76d0::/64;
                allow ::1/128;
                allow 172.22.99.0/24;
                allow 172.20.72.0/21;
                allow 127.0.0.0/8;
                deny all;
              '';
            };
          };
        };

        prometheus.exporters.node = {
          enable = true;
          enabledCollectors = [ "ethtool" "systemd" ];
          listenAddress = "127.0.0.1";
          openFirewall = true;
          port = 9101;
        };
      })
    ];
  };
}
