{ config, ... }:

{
  microvm.mem = 2048;
  c3d2.deployment.server = "server10";

  networking.hostName = "blogs";

  # See secrets/hosts/blogs for the .env file with all settings
  services.plume = {
    enable = true;
    envFile = config.sops.secrets."plume/env".path;
  };

  sops = {
    age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "plume/env".owner = config.systemd.services.plume.serviceConfig.User;
    };
  };

  services.nginx = {
    enable = true;
    virtualHosts."blogs.c3d2.de" = {
      forceSSL = true;
      enableACME = true;
      locations."/".proxyPass = "http://localhost:7878";
    };
  };

  system.stateVersion = "22.05";
}
