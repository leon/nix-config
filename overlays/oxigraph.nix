{ lib
, rustPlatform
, fetchFromGitHub
, pkg-config
, llvmPackages
}:

rustPlatform.buildRustPackage rec {
  pname = "oxigraph";
  version = "0.3.11";

  src = fetchFromGitHub {
    owner = pname;
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-7KbDZKKJPk3QTp4siIbdB6xKbslw73Lhc7NoeOuA0Og=";
    fetchSubmodules = true;
  };

  cargoSha256 = "sha256-Yqn6hwejg6LzcqW0MiUN3tqrOql6cpu/5plaOz+2/ns=";

  nativeBuildInputs = [
    pkg-config llvmPackages.clang
  ];
  LIBCLANG_PATH = "${llvmPackages.libclang.lib}/lib";

  preConfigure = ''
    cd server
  '';
  postBuild = ''
    cd ..
  '';

  doCheck = false;
}
