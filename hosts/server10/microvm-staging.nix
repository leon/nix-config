{ config, pkgs, ... }:
let
  microvms = {
    staging-data-hoarder.flakeref = "git+https://github.com/tlm-solutions/nix-config";
    borken-data-hoarder.flakeref = "git+file:///tmp/dump-dvb-borken";
    tram-borzoi.flakeref = "git+file:///tmp/dump-dvb-borzoi";
  };
  realizeFlake = with pkgs; "${writeScriptBin "realize-flake" ''
    #! ${runtimeShell} -e
    set -x
    NAME=$1

    if [ $NAME = "borken-data-hoarder" ]; then
       SRC_NIX_CONFIG=https://github.com/dump-dvb/nix-config.git
       BRANCH=borken
       DIR_NIX_CONFIG=dump-dvb-borken

    elif [ $NAME = "tram-borzoi" ]; then
       SRC_NIX_CONFIG=https://github.com/dump-dvb/nix-config.git
       BRANCH=borken
       DIR_NIX_CONFIG=dump-dvb-borzoi

    elif [ $NAME = "staging-data-hoarder" ]; then
       echo "staging-data-hoarder: no need for flake magic"
       echo "Deploying staging-data-hoarder from github"
       exit 0
    else
       echo "Do not know what to do"
       exit 0
    fi

    cd /tmp
    if [ -d $DIR_NIX_CONFIG ]; then
       cd $DIR_NIX_CONFIG
       git fetch origin
       git reset --hard origin/$BRANCH
    else
       git clone -b $BRANCH --single-branch $SRC_NIX_CONFIG $DIR_NIX_CONFIG
       cd $DIR_NIX_CONFIG
    fi

    git config user.email "grisha@tlm.solutions"
    git config user.name "Flake McUpdater"
    nix flake update --commit-lock-file

  ''}/bin/realize-flake";
in
{
  microvm.autostart = builtins.attrNames microvms;

  systemd.services = {

    "microvm-update@" = {
      description = "Update MicroVMs automatically";
      after = [ "network-online.target" ];
      unitConfig.ConditionPathExists = "/var/lib/microvms/%i";
      serviceConfig = {
        Type = "oneshot";
      };
      path = with pkgs; [ nix git ];
      environment.HOME = config.users.users.root.home;
      scriptArgs = "%i";
      script = ''
        NAME=$1
        ${realizeFlake} $NAME
        /run/current-system/sw/bin/microvm -Ru $NAME
      '';
    };

  } // builtins.foldl' (services: name: services // {
    "microvm-create-${name}" = {
      description = "Create MicroVM ${name} automatically";
      wantedBy = [ "microvms.target" ];
      after = [ "network-online.target" ];
      before = [
        "microvm-tap-interfaces@${name}.service"
        "microvm-virtiofsd@${name}.service"
      ];
      unitConfig.ConditionPathExists = "!/var/lib/microvms/${name}";
      serviceConfig.Type = "oneshot";
      path = with pkgs; [ nix git ];
      environment.HOME = config.users.users.root.home;
      scriptArgs = "${name}";
      script = ''
        ${realizeFlake} ${name}
        /run/current-system/sw/bin/microvm -c ${name} -f "${microvms.${name}.flakeref}"
      '';
    };
  }) {} (builtins.attrNames microvms);

  systemd.timers = builtins.foldl' (timers: name: timers // {
    "microvm-update-${name}" = {
      wantedBy = [ "timers.target" ];
      timerConfig = {
        Unit = "microvm-update@${name}.service";
        # three times per hour
        OnCalendar = "*:0,20,40:00";
        Persistent = true;
      };
    };
  }) {} (builtins.attrNames microvms);
}
