{ zentralwerk, config, ... }:

{
  system.stateVersion = "22.05";

  c3d2 = {
    deployment.server = "server10";
    hq.statistics.enable = true;
  };

  networking.hostName = "ticker";

  services = {
    backup.paths = [ "/var/lib/ticker/" ];

    nginx = {
      enable = true;
      virtualHosts."ticker.c3d2.de" = {
        forceSSL = true;
        enableACME = true;
        locations."/".proxyPass = "http://localhost:8400";
      };
    };

    ticker = {
      updateInterval = "hourly";
      config.calendars = {
       c3d2 = {
          url = "https://c3d2.de/ical.ics";
          color = "#BFA73F";
        };
        dresden-science = {
          url = "https://www.dresden-science-calendar.de/calendar/de/iCalSync.ics";
          color = "#00007F";
        };
        gruenes-brett = {
          url = "https://dresden.gruenesbrett.net/ical/all/";
          color = "#00BF00";
        };
        stura-htw = {
          url = "http://www.stura.htw-dresden.de/events/aggregator/ics_view";
          color = "#BF3FA7";
          defaults = {
            location = "StuRa, HTW Dresden";
            url = "https://stura.htw-dresden.de/";
          };
        };
        malobeo = {
          url = "https://malobeo.org/events/list/?ical=1&tribe_display=all";
          color = "#FF3F3F";
        };
        hicknhack = {
          url = "https://www.google.com/calendar/ical/grhnk1uaotql6gv2dkf9ldmqjc%40group.calendar.google.com/public/basic.ics";
          color = "#A700A7";
        };
        # palaissommer = {
        #   url = "https://palaissommer.de/programm/?event=all";
        #   color = "#7F003F";
        # };
        kreta = {
          url = "https://www.kreta-dresden.org/kreta.ics";
          color = "#BF3F7F";
        };
        zentralwerk = {
          url = "https://ics.teamup.com/feed/ksayh65fgotv2prcas/0.ics";
          color = "#FF3F3F";
          defaults = {
            location = "Zentralwerk, Riesaer Str. 32";
            url = "https://www.zentralwerk.de";
          };
        };
        and = {
          url = "https://a-dresden.org/veranstaltungen/liste/?ical=1";
          color = "#FF7F00";
        };
        rosenwerk-home = {
          url = "https://www.google.com/calendar/ical/bj85d742g31mgkblbaiusmk3s8%40group.calendar.google.com/public/basic.ics";
          color = "#DF003F";
          defaults = {
            location = "Konglomerat e.V., Jagdweg 1-3";
            url = "https://konglomerat.org/programm.html";
          };
        };
        rosenwerk-kultur = {
          url = "https://www.google.com/calendar/ical/93enn926ddhgr79hnqp83ipj3g%40group.calendar.google.com/public/basic.ics";
          color = "#BF001F";
          defaults = {
            location = "Konglomerat e.V., Jagdweg 1-3";
            url = "https://konglomerat.org/programm.html";
          };
        };
        haengemathe = {
          url = "https://club-haengemathe.de/termine.ics";
          color = "#FF7F3F";
        };
        bitsundbaeumedresden = {
          url = "https://dresden.bits-und-baeume.org/termine.ics";
          color = "#3FBF3F";
        };
        exma-stuta = {
          url = "https://www.exmatrikulationsamt.de/ics/v1/stuta.ics";
          color = "#7F7FFF";
        };
        exma-nawa = {
          url = "https://www.exmatrikulationsamt.de/ics/v1/nawa.ics";
          color = "#3F3FBF";
        };
        ratsinfo = {
          url = "https://github.com/offenesdresden/dresden-ratsinfo/raw/master/meetings.ics";
          color = "#BFBF3F";
        };
        filmnaechte = {
          url = "https://dresden.filmnaechte.de/veranstaltung?tx_events_frontend%5Baction%5D=ical&tx_events_frontend%5Bcontroller%5D=Event&tx_events_frontend%5Bevent%5D=26&cHash=d38f8b752d27504bb578a1d8a361721a";
          color = "#16182c";
        };
        riesa-efau = {
          url = "https://scrape.hq.c3d2.de/riesa-efau-kalender.ics";
          color = "#7FBF7F";
        };
        gartennetzwerk = {
          url = "https://www.dresden-pflanzbar.de/?plugin=all-in-one-event-calendar&controller=ai1ec_exporter_controller&action=export_events&no_html=true";
          color = "#3FAF00";
        };
        ffdd = {
          url = "https://mobilizon.envs.net/@ffdd/feed/ics";
          color = "#AFAF00";
        };
        dhmd = {
          url = "https://scrape.hq.c3d2.de/dhmd-veranstaltungen.ics";
          color = "#E00728";
        };
        ds22 = {
          url = "https://talks.datenspuren.de/ds22/schedule/export/schedule.ics";
          color = "#40A940";
        };
        medienkulturzentrum = {
          url = "https://scrape.hq.c3d2.de/mkz-programm.ics";
          color = "#DF3FBF";
        };
        ddosug = {
          url = "https://www.meetup.com/de-DE/ddos-usergroup/events/ical/";
          color = "#001F3F";
        };
        diwo = {
          url = "https://pretix.eu/diwoweek/events/ical/?locale=de";
          color = "#723465";
        };
        gaertjen = {
          url = "https://gaertjen.de/kalender/?ical=1";
          color = "#3FDFAF";
        };
        klimacamp-dresden = {
          url = "https://spaceboyz.net/~astro/KlimacampDresden.ics";
          color = "#7B996A";
        };
        gefilte-fest-dresden = {
          url = "http://gefilte-fest-dresden.de/feed/my-calendar-google/";
          color = "#4B693A";
        };
        dresden-versammlungen = {
          url = "https://scrape.hq.c3d2.de/dresden-versammlungen.ics";
          color = "#2F2700";
        };
        azconni = {
          url = "https://scrape.hq.c3d2.de/azconni.ics";
          color = "#CF3F00";
        };
        impact-hub = {
          url = "https://dresden.impacthub.net/calendar/liste/?hide_subsequent_recurrences=0&ical=1";
          color = "#3F2FCF";
          defaults.location = "Impact Hub Hbf, Bayrische Straße 8";
        };
        kursiv = {
          url = "https://www.kursif.eu/termine/events.ics";
          color = "#6F5FFF";
        };
        wirtschaftsjunioren-dresden = {
          url = "https://www.verbandonline.org/wj-dresden/?ical";
          color = "#7F00AF";
        };
        zoo-vortraege = {
          url = "https://zoofreunde-dresden.de/index.php?option=com_dpcalendar&task=ical.download&id=41";
          color = "#AFFFAF";
        };
        zoo-veranstaltungen = {
          url = "https://zoofreunde-dresden.de/index.php?option=com_dpcalendar&task=ical.download&id=40";
          color = "#AFFFAF";
        };
        radclub-veranstaltungen = {
          url = "https://calendar.google.com/calendar/ical/7829omgggk4ksehag9l4eiuvo0@group.calendar.google.com/public/basic.ics";
          color = "#FF003F";
        };
        # too many internal events
        # hzdr = {
        #   url = "https://www.hzdr.de/db/calendar.data.ics";
        #   color = "#2F3FAF";
        # };
        uniklinikum = {
          url = "https://www.uniklinikum-dresden.de/de/@@event_listing_ical?mode=future";
          color = "#AF3F2F";
          defaults = {
            location = "Uniklinikum";
            url = "https://www.uniklinikum-dresden.de/";
          };
        };
        pfsr = {
          url = "https://pfsr-web.phy.tu-dresden.de/drupal/calendar/*/export.ics";
          color = "#AF5F5F";
        };
        zukunftsstadt = {
          url = "https://www.zukunftsstadt-dresden.de/veranstaltungen/?ical=1";
          color = "#FFFF5F";
        };
        kunsthaus = {
          url = "https://scrape.hq.c3d2.de/kunsthaus.ics";
          color = "#000000";
        };
        kultursommer = {
          url = "https://kultursommerdresden.de/kalender/?ical=1";
          color = "#9CC870";
        };
        hfmdd = {
          url = "https://scrape.hq.c3d2.de/hfmdd.ics";
          color = "#648001";
        };
        hfbk-dresden = {
          url = "https://scrape.hq.c3d2.de/hfbk-dresden.ics";
          color = "#3366FF";
        };
        ezag = {
          url = "https://tu-dresden.de/ing/informatik/sya/professur-fuer-betriebssysteme/die-professur/termine/echtzeit-ag/ics_view";
          color = "#002557";
        };
        dresdenjs = {
          url = "https://www.meetup.com/de-DE/DresdenJS-io-JavaScript-User-Group/events/ical/";
          color = "#F65858";
        };
        cpp-ug = {
          url = "https://www.meetup.com/de-DE/cpp-ug-dresden/events/ical/";
          color = "#FFCF9F";
        };
        tjg-dresden = {
          url = "https://scrape.hq.c3d2.de/tjs-dresden.ics";
          color = "#9F9FFF";
        };
        software-engineering-community = {
          url = "https://www.meetup.com/software-engineering-community/events/ical";
          color = "#5F1F7F";
        };
        slubmakerspace = {
          url = "https://www.meetup.com/slubmakerspacemeetup/events/ical";
          color = "#7F0F1F";
        };
        php-usergroup = {
          url = "https://www.meetup.com/php-usergroup-dresden/events/ical";
          color = "#FFAF3F";
        };
        dresden-kulturstadt = {
          url = "https://scrape.hq.c3d2.de/dresden-kulturstadt.ics";
          color = "#29333F";
        };
        kino-im-kasten = {
          url = "https://www.kino-im-kasten.de/ical";
          color = "#550000";
          defaults.location = "Kino im Kasten, August-Bebel-Str. 20";
        };
        juedische-woche-dresden = {
          url = "https://juedische-woche-dresden.de/events/?ical=1";
          color = "#7AADAE";
        };
        csd-dresden = {
          url = "https://www.csd-dresden.de/events/?ical=1";
          color = "#C36";
        };
        pjr-dresden = {
          url = "https://pjr-dresden.de/events/?ical=1&tribe_display=list";
          color = "#0091D6";
        };
        nabu = {
          url = "https://scrape.hq.c3d2.de/nabu.ics";
          color = "#4A721A";
        };
        fau-dresden = {
          url = "https://dd.fau.org/events.ics";
          color = "#7F1F0F";
        };
        # # has very few links :(
        # terminal-dresden = {
        #   url = "https://export.kalender.digital/ics/0/terminaldresden/dresdnerterminal.ics?past_months=1&future_months=2";
        #   color = "#4F2F4F";
        # };
        dgfk = {
          url = "https://dresden.dgfk.net/?plugin=all-in-one-event-calendar&controller=ai1ec_exporter_controller&action=export_events";
          color = "#A0C269";
        };
        museen-dresden = {
          url = "https://scrape.hq.c3d2.de/museen-dresden.ics";
          color = "#943008";
        };
        criticalmass = {
          url = "https://scrape.hq.c3d2.de/criticalmass.ics";
          color = "#3FBF9F";
        };
        kosmotique = {
          url = "https://scrape.hq.c3d2.de/kosmotique.ics";
          color = "#dfd";
        };
        gruene-jugend = {
          url = "https://gj-sachsen.de/?plugin=all-in-one-event-calendar&controller=ai1ec_exporter_controller&action=export_events&no_html=true&ai1ec_tag_ids=667";
          color = "#268500";
        };
        jkpev = {
          url = "https://www.jkpev.de/(/?:events)/(/?:category)/(/?:%5B/%5D&ical=1";
          color = "#ff9500";
        };
        club-aquarium = {
          url = "https://www.club-aquarium.de/events/index.ics";
          color = "#3F7FFF";
        };
        metaknoten = {
          url = "https://www.metaknoten.net/de/veranstaltungen.ics";
          color = "#040a25";
        };
        gag18 = {
          url = "https://calendar.google.com/calendar/ical/kellerklubgag18%40gmail.com/public/basic.ics";
          color = "#640909";
          defaults = {
            location = "Kellerclub GAG 18, Fritz-Löffler-Straße 16";
            url = "https://www.gag-18.com/veranstaltungen-1/veranstaltungskalender/";
          };
        };
      };
    };
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "restic/password".owner = "root";
      "restic/repository/server8".owner = "root";
    };
  };
}
