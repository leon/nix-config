_:

{
  imports = [
    ./hardware-configuration.nix
  ];

  c3d2 = {
    baremetal = true;
    hq.statistics.enable = true;
  };

  boot = {
    initrd = {
      availableKernelModules = [ "igb" ];
      network.ssh.enable = true;
    };
    loader.systemd-boot.enable = true;
    kernelParams = [
      "preempt=none"
      # No server/router runs any untrusted user code
      "mitigations=off"
    ];
    tmpOnTmpfs = true;
    tmpOnTmpfsSize = "80%";
  };

  disko.disks = [ {
    device = "/dev/disk/by-id/ata-Samsung_SSD_860_EVO_1TB_S3Z9NB0M203733F";
    name = "ssd0";
  } ];

  networking = {
    hostName = "server6";
    hostId = "8a3ba5a7";
  };

  simd.arch = "ivybridge"; # E5-2690 v2

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets."machine-id" = {
      mode = "444";
      path = "/etc/machine-id";
    };
  };

  skyflake.nomad.client.meta."c3d2.cpuSpeed" = "5";

  system.stateVersion = "22.11";
}
